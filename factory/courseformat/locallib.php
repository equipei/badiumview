<?php
require_once("$CFG->dirroot/theme/badiumview/locallib/course/contentlib.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/course/accesslib.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/utildata.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/pluginconfig.php");
require_once("$CFG->dirroot/theme/badiumview/factory/basedata.php");

class theme_badiumview_factory_courseformat_locallib extends  theme_badiumview_factory_basedata {
  function __construct() {
		parent::__construct();
	}

public function init() { 
		global $COURSE;
		global $USER;
		$themeconfig=new theme_badiumview_pluginconfig();
		$this->getData()->firsttopicaspresentation=$themeconfig->getValue('factorycourseformatfirsttopicaspresentation');
		$fitterparamtopic=array('courseid'=>$COURSE->id,'visible'=>1,'available'=>1,'userid'=>$USER->id,'addactivities'=>1,'addactivitiesitemhtml'=>1,'countaccess'=>1,'firsttopicaspresentation'=>$this->getData()->firsttopicaspresentation);
		$this->getData()->topics=$this->getTopics($fitterparamtopic);
		//$this->getData()->topics=$this->manageTopicToShow($fitterparamtopic,$listtopics);
		$this->showVariables();
		
	}

	/**
     * get list of course topics
	 
     *
     * @param $courseid
     * @param null $section
     * @return array|null
     * @throws dml_exception
     */
    public function getTopics($param) {
		 $utildata=new theme_badiumview_utildata();
      
	    $userid   = $utildata->getVaueOfArray($param,'userid');
        $courseid = $utildata->getVaueOfArray($param,'courseid');
		$addactivities=$utildata->getVaueOfArray($param,'addactivities');
		$firsttopicaspresentation=$utildata->getVaueOfArray($param,'firsttopicaspresentation');
		
		$contentlib=new theme_badiumview_course_contentlib();
		
          //$param=array('courseid'=>$courseid,'visible'=>1);
          $list = $contentlib->get_topics($param);
		  $atlist=$contentlib->get_full_by_course($courseid);
		
		 $param['list']=$list;
          $list=$contentlib->add_topic_config($param);
		  
		   
		  
		  //get_list_item
		  if($addactivities){ 
			  $listnew=array();
			  $activities=$this->getActivities($param);
			  
			  //last id activity accessed
			  $accesslib=new theme_badiumview_course_accesslib();
			  $listlastaccessativities= $accesslib->get_lastaccess_ativities($courseid,$userid);
			  $lastactivityidaccessed=$accesslib->get_ativityid_lastaccessed($listlastaccessativities);
			  $listlastaccessativities= $accesslib->get_lastaccess_ativities_array_id_key($listlastaccessativities);
		
			  foreach ($list as $trow) {
				$tsection=$utildata->getVaueOfArray($trow,'section'); 
				$activitiesnew=array();
				$countactivity=0; 
				$countactivitycompleted=0;
				$progresspercentnumber=0;
				$progresspercentnumberformat=0;
				$countactivityinprogress=0;
				$haslastactivityaccessed=0;
				foreach ( $activities as $ka =>$arow) {
					$activityid=$utildata->getVaueOfArray($arow,'id');
					$asection=$utildata->getVaueOfArray($arow,'section');
					$completed=$utildata->getVaueOfArray($arow,'completed');
					$tstatus=$utildata->getVaueOfArray($arow,'status');
					$completionview=$utildata->getVaueOfArray($arow,'completionview');
					$lastaccess=$utildata->getVaueOfArray($listlastaccessativities,$activityid);
					$arow['lastaccess']=$lastaccess;
						 if($tsection==$asection){ 
							if($completionview > 0){
								$countactivity++;
								if($completed){$countactivitycompleted++;}
								if($tstatus=='inprogress'){$countactivityinprogress++;}
							}
							//check if last accessed
							if($activityid==$lastactivityidaccessed){$haslastactivityaccessed=1;}
							$activitiesnew[$ka]=$arow;
						}
					  
					  
					}
				 $trow['activities']=$activitiesnew;
				 $trow['lastaccessed']=$haslastactivityaccessed; 
				 $trow['showactivities']="";
				 if($haslastactivityaccessed){$trow['showactivities']=" in ";}
				 //percent progress
				
				if($countactivity >=0 && $countactivitycompleted >=0 && $countactivity >= $countactivitycompleted){
					if($countactivity==0){$progresspercentnumber=0;}
					else{
						$progresspercentnumber= $countactivitycompleted*100/$countactivity;
						$progresspercentnumberformat=number_format($progresspercentnumber, 2, ',', '.');
					}
              
				} 
				$status='';  
				$trow['completed']=0;
				if($progresspercentnumber==100){$trow['completed']=1;}
				$trow['countactivity']=$countactivity;
				$trow['countactivitycompleted']=$countactivitycompleted;
				$trow['progresspercentnumber']=$progresspercentnumber;
				$trow['progresspercentnumberformat']=$progresspercentnumberformat;
				//$trow['progresspercentinfo']="Atividades concluídas  $countactivitycompleted de $countactivity ($progresspercentnumberformat%)";
				$trow['progresspercentinfo']=" $countactivitycompleted de $countactivity ($progresspercentnumberformat%)";
				
				//status
				
				$trow['status']=''; //completed | inprogress | notstarted
				$trow['statusicon']='';
				if($progresspercentnumber==100){$trow['status']='completed';$trow['statusicon']='fa fa-check-circle-o';}
				else if($countactivityinprogress > 0){$trow['status']='inprogress'; $trow['statusicon']='fa fa-circle-thin';}
				else if($countactivityinprogress = 0){$trow['status']='notstarted';$trow['statusicon']='fa fa-circle-thin';}
				 array_push($listnew, $trow);	 
			  }
			  
			  $list=$listnew;
		  }
		 
		/*echo "<pre>";
		print_r($list);
		echo "</pre>";exit;*/
		  return $list;
		
	}
	/**
     * manage list of topic avalible to keep in list
     *
     * @return array
     * @throws coding_exception
     * @throws dml_exception
     */
    public function manageTopicToShow($param,$list) {
		if(!is_array($list)){return null;}
		$available = $this->getUtildata()->getVaueOfArray($param,'available');
		if(!$available){return $list;}
		$listnew=array();
		foreach ($list as $trow) {
			$tavailable = $this->getUtildata()->getVaueOfArray($trow,'available');
			 if($tavailable){array_push($listnew, $trow);}
				 
		}
		return $listnew;
	}
	
	/**
     * Get course activities.
     *
     * @return array
     * @throws coding_exception
     * @throws dml_exception
     */
    public function getActivities($param) {
		global $CFG;
        $utildata=new theme_badiumview_utildata();
        $userid   = $utildata->getVaueOfArray($param,'userid');
        $courseid = $utildata->getVaueOfArray($param,'courseid');
		$section = $utildata->getVaueOfArray($param,'section');
        $addactivitiesitemhtml=$utildata->getVaueOfArray($param,'addactivitiesitemhtml');
		$countaccess=$utildata->getVaueOfArray($param,'countaccess');
		
		
		if(empty($courseid)){return array();}
        
        $dcourse     = new stdClass();
        $dcourse->id = $courseid;
        $cmodinfo     = get_fast_modinfo($dcourse);

        $contentlib=new theme_badiumview_course_contentlib();
        $activities =$contentlib->get_list($courseid, $section);
        $activities = $this->castListactivitiesToArray($activities);
        
        $listactivitiescompletd = $contentlib->get_list_completed($userid, $courseid);
        $listactivityanableds   = $contentlib->get_enable_ativities($courseid);
        
		$listactivitieshtml=array();
        if($addactivitiesitemhtml){
			$listactivitieshtml=$contentlib->get_list_item($courseid, $section);
	    }
	   
		$activitiesnew=array();
		
		//count access
		$listactivitiseaccess=array();
		if($countaccess){
			$listactivitiseaccess=$contentlib->get_ativities_access_by_user($courseid,$userid);
			$listactivitiseaccessnew=array();
			foreach ($listactivitiseaccess as $lrow) {
				$acmi=$lrow->cmid;
				$cmidcountaccess=$lrow->countaccess;
				$listactivitiseaccessnew[$acmi]=$cmidcountaccess;
			}
			$listactivitiseaccess=$listactivitiseaccessnew;
		}
        foreach ($activities as $row) {
            $id = $utildata->getVaueOfArray($row,'id');
			$visible = $utildata->getVaueOfArray($row,'visible');
			$visibleold = $utildata->getVaueOfArray($row,'visibleold');
			$module = $utildata->getVaueOfArray($row,'module');
			$name = $utildata->getVaueOfArray($row,'name');
			$completionview = $utildata->getVaueOfArray($row,'completionview');
            $kidnumber=$utildata->getVaueOfArray($row,'idnumber');
			$kidnumber=trim($kidnumber);
			if($kidnumber=='' || $kidnumber==null){$kidnumber= $id;}
			
			//add url
			$url=$CFG->httpswwwroot."/mod/$module/view.php?id=$id";
			$row['url']=$url;
			
			//add link
			$link="<a href=\"$url\">$name</a>";
			$row['link']=$link;
			
			$row['html']=null;
			 if($addactivitiesitemhtml && isset($listactivitieshtml[$id])) {
				 $row['html']=$listactivitieshtml[$id];
			 }
			 //icon 
			 $row['urlicon']=$CFG->httpswwwroot."/mod/$module/pix/icon.svg";
			
			$available=0;
			if (array_key_exists($id,$listactivityanableds)){ $available = 1;}
			
            $cml = $cmodinfo->get_cm($id);
			$row['uservisible'] = $cml->uservisible;
			//$row['available'] = $cml->available;
			$row['available']=$available;
			$row['availableinfo'] =\core_availability\info::format_info($cml->availableinfo, $dcourse);
						
			$row['locked'] = 0;
            if (!$cml->uservisible || !$available) {
                $row['locked'] = 1;
            }
            if (array_key_exists($id, $listactivitiescompletd)) {
                    $row['completed']=1;
            } else {
                $row['completed']=0;
				if($completionview==0){$row['completed']=-1;}
            }
				
			
			$row['countaccess']=null;
			
			//count access
			if (array_key_exists($id, $listactivitiseaccess)) {$row['countaccess']=$listactivitiseaccess[$id];}
			
			//status
			$row['status']=null;
			$row['statuslabel']=null;
			$row['statusicon']=null;
			$countaccess=$utildata->getVaueOfArray($row,'countaccess');
			$completed=$utildata->getVaueOfArray($row,'completed');
			$locked=$utildata->getVaueOfArray($row,'locked');
			
			if($completed==1){
				$row['status']='completed';
				$row['statuslabel']=get_string('completed', 'theme_badiumview');
				$row['statusicon']='fa fa-check-circle';
			}else if($completed==-1){
				$row['status']='none';
				$row['statuslabel']=get_string('withoutprogressconfig', 'theme_badiumview');
				$row['statusicon']='fa fa-times-circle-o';
				
			}
			else if ($countaccess > 0){
				$row['status']='inprogress';
				$row['statuslabel']=get_string('inprogress', 'theme_badiumview');
				$row['statusicon']='fa fa-circle';
			}else if ($countaccess == 0 ){
				$row['status']='notstarted';
				$row['statuslabel']=get_string('notstarted', 'theme_badiumview');
				$row['statusicon']='fa fa-circle';
			}
			
			if($locked){ 
				$row['status']='loked';
				$row['statuslabel']=get_string('notstarted', 'theme_badiumview');
				$row['statusicon']='fa fa-lock';
				
			} 
			$activitiesnew[$kidnumber]=$row;
			 //array_push($activitiesnew, $row);
			
        }
		/*echo "<pre>";
		print_r($activitiesnew);
		echo "</pre>";exit;*/
        return $activitiesnew;
    }
	
	/** review it
     * get information of activity status
     *
     * @param $topicitem - array with info of topic
	 * @param $activityitem - array  with info of activity
	 * @param $type - iconcssclass | cssclass | statuslabel 
     * @return array
     */
    private function getAtivityStatusShow($topicitem,$activityitem,$type) {
        if($type=='iconcssclass'){
			
		}
		
        return null;
    }
	
	/**
     * Cast list activities to array.
     *
     * @param $result
     * @return array
     */
    private function castListactivitiesToArray($result) {
        $array = array();
        foreach ($result as $key => $value) {
            if (is_array($value)) {
                $array[$key] = $this->castListactivitiesToArray($value);
            }
            else {
                if (is_object($value)) {
                    $array[$key] = $this->castListactivitiesToArray($value);
                } else {
                    $array[$key] = $value;
                }
            }
        }
        return $array;
    }
	
		/**
     * Cast getCourseProgress
     *
     * @param $result
     * @return array
     */
     function getCourseProgress($listtopicwithactivities) {
		 $utildata=new theme_badiumview_utildata();
		  $coursecountactivity=0; 
		  $coursecountactivitycompleted=0;
		  $courseprogresspercentnumber=0;
		  $courseprogresspercentnumberformat=0;
		  
		  
		  foreach ($listtopicwithactivities as $trow) {
			 
			  $activities=$utildata->getVaueOfArray($trow,'activities');
			  foreach ( $activities as $arow) {
					$completed=$utildata->getVaueOfArray($arow,'completed');
					if($completed >=0){$coursecountactivity++;}
					if($completed >0){$coursecountactivitycompleted++;}
				}
		
		  }
		  
		  //course percent progress
				if($coursecountactivity >=0 && $coursecountactivitycompleted >=0 && $coursecountactivity >= $coursecountactivitycompleted){
					if($coursecountactivity==0){$courseprogresspercentnumber=0;}
					else{
						$courseprogresspercentnumber= $coursecountactivitycompleted*100/$coursecountactivity;
						$courseprogresspercentnumberformat=number_format($courseprogresspercentnumber, 2, ',', '.');
					}
              
				}
			$icourse=array();
			$icourse['countactivity']=$coursecountactivity;
			$icourse['countactivitycompleted']=$coursecountactivitycompleted;
			$icourse['progresspercentnumber']=$courseprogresspercentnumber;
			$icourse['progresspercentnumberformat']=$courseprogresspercentnumberformat;
			$icourse['progresspercentinfo']=" $coursecountactivitycompleted de $coursecountactivity ($courseprogresspercentnumberformat%)";
			
			return $icourse;
	}
	
			/** 
     * show var
        */
     function showVariables() {
		 $badiumviewcoreparam = optional_param('_badiumviewcoreparam',null,PARAM_TEXT); 
		 if($badiumviewcoreparam=='phpdata'){
			echo "<pre>";
			print_r($this->getData());
			echo "</pre>";
		 }
		  if($badiumviewcoreparam=='jsondata'){
			echo "<pre>";
			print_r(json_encode($this->getData()));
			echo "</pre>";
		 }
	 }
	 
	 
	 function activitiesViewBootstrap($param){     
		$rows=$this->getUtildata()->getVaueOfArray($param,'rows');
		$bootstrapgridclass=$this->getUtildata()->getVaueOfArray($param,'bootstrapgridclass');
		$containerid=$this->getUtildata()->getVaueOfArray($param,'containerid');
		$maxcardperrow=$this->getUtildata()->getVaueOfArray($param,'maxcardperrow');
		//$isteachear=$this->getUtildata()->getVaueOfArray($param,'isteachear');
	
		$gridcont=1;
		$cont=0;
		$html="";
		$html.="<div id=\"$containerid\">";  
		$starrow="<div class=\"row\" >";
		$endrow="</div>";

    
		global $CFG;
      foreach ($rows as $row) {
         
		   $atname=$this->getUtildata()->getVaueOfArray($row,'name');
		   $description=$this->getUtildata()->getVaueOfArray($row,'intro');
		   $auservisible=$this->getUtildata()->getVaueOfArray($row,'uservisible');
		   $aavailable=$this->getUtildata()->getVaueOfArray($row,'available');
		   $atlocked=$this->getUtildata()->getVaueOfArray($row,'locked');
		   $aturl=$this->getUtildata()->getVaueOfArray($row,'url');
		   $link=$this->getUtildata()->getVaueOfArray($row,'link');
		   $atstatus=$this->getUtildata()->getVaueOfArray($row,'status');
		   $aturlicon=$this->getUtildata()->getVaueOfArray($row,'urlicon');
		   $atstatuslabel=$this->getUtildata()->getVaueOfArray($row,'statuslabel');
		   $atstatusicon=$this->getUtildata()->getVaueOfArray($row,'statusicon');
						
			
			$urlcourse= $CFG->httpswwwroot."/course/view.php?id=$courseid";
			$linkcourse="<a href=\"$urlcourse\"></a>";
			  if($gridcont==1){$html.= $starrow;}
              $html.="<div class=\"$bootstrapgridclass\">";
             
			  $html.="<div class=\"card\">";
			  $html.="<h5 class=\"card-header\">$atname</h5>";
			  $html.="<div class=\"card-body\">";
			  //$html.="<h5 class=\"card-title\">Special title treatment</h5>";
			  $html.="<p class=\"card-text\">$description</p>";
			  $html.="</div>";
			  $html.=" <div class=\"card-footer text-muted\"><a href=\"$aturl\" class=\"btn btn-primary\">Acessar</a></div>";
			  $html.="</div>"; //end card 
			 $html.=" </div>"; //end  col
              $gridcont++; 
              $cont++; 
              if($gridcont > $maxcardperrow){
                  $gridcont=1;
                  $html.= $endrow;
              } else if($cont==count($rows)){$html.=   $endrow;}
              
        
          } //end foreach ($litem as $item) {
       $html.="</div>";
       return  $html;
    }
} 
