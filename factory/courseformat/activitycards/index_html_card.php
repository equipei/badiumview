<div id="badiu-courseformat">
	<div class="row">
	<?php 
	$cont=0;
	foreach ($badiumfview->getData()->topics as $itemtopic){
		$tavailable=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'available');
		$tavailableinfo=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'availableinfo');
		$tuservisible=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'uservisible');
		$tvisible=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'visible');
		
		$showsection = $tuservisible || ($tvisible && !$tavailable && !empty($tavailableinfo));
		$section=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'section');
		//echo "available: $section | $available ffffffffffff<hr>";
		//if($available){$cont++;}
		$cont++;
		$show=true;
		if($badiumfview->getData()->firsttopicaspresentation && $cont==1){$show=false;}
		if($show && $showsection){
		$topiciconlock=''; 
		if(!$tuservisible){$topiciconlock= '<i class="fa fa-lock" aria-hidden="true"></i>';}
		else {$topiciconlock=''; }
		?>
	   <div class="col-12">
		<div class="card">
			<div class="card-header">
			   <div class="row">
					<div class="col-md-8">  <a data-toggle="collapse" href="#collapse<?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'section'); ?>" class="topic"> <?php echo $topiciconlock; ?> <i class="fa fa-list-alt" aria-hidden="true"></i> <?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'name'); ?></a></div>
					<div class="col-md-4 progressinfo<?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'status'); ?> text-right "> <?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'progresspercentinfo'); ?> <i class="<?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'statusicon'); ?> " ></i></div>
			   </div> 
			</div>   
			<div class="card-body">			
			   <div class="list-group-topics">
				<ul class="list-group1">
					<?php 
					$bmvlactivities=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'activities');
					
					echo $badiumfview->activitiesViewBootstrap(array('rows'=>$bmvlactivities,'bootstrapgridclass'=>'col-xs-12','containerid'=>'coursecardtrail1','maxcardperrow'=>1)); //one cart 0px - 768px
					echo $badiumfview->activitiesViewBootstrap(array('rows'=>$bmvlactivities,'bootstrapgridclass'=>'col-md-6','containerid'=>'coursecardtrail2','maxcardperrow'=>2)); //thwo cart 768px - 1100px
					echo $badiumfview->activitiesViewBootstrap(array('rows'=>$bmvlactivities,'bootstrapgridclass'=>'col-md-4','containerid'=>'coursecardtrail3','maxcardperrow'=>3)); //tree cart 1101px - 1400px
					echo $badiumfview->activitiesViewBootstrap(array('rows'=>$bmvlactivities,'bootstrapgridclass'=>'col-lg-3','containerid'=>'coursecardtrail4','maxcardperrow'=>4));  //four cart >= 1401px 
					?> 
					
				</ul>
			<br /> </div>	
			</div>  
		</div><br />
	 </div <!--end col-12-->
	<?php }?> <!--end if whow for first topic -->
	<?php }?> <!--end foreach topics-->
	</div> 
	 
	
</div>