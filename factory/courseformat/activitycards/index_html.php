<div id="badiu-courseformat">
	<div class="row">
	<?php 
	$cont=0;
	foreach ($badiumfview->getData()->topics as $itemtopic){
		$tavailable=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'available');
		$tavailableinfo=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'availableinfo');
		$tuservisible=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'uservisible');
		$tvisible=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'visible');
		
		$showsection = $tuservisible || ($tvisible && !$tavailable && !empty($tavailableinfo));
		$section=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'section');
		//echo "available: $section | $available ffffffffffff<hr>";
		//if($available){$cont++;}
		$cont++;
		$show=true;
		if($badiumfview->getData()->firsttopicaspresentation && $cont==1){$show=false;}
		if($show && $showsection){
		$topiciconlock=''; 
		if(!$tuservisible){$topiciconlock= '<i class="fa fa-lock" aria-hidden="true"></i>';}
		else {$topiciconlock=''; }
		?>
	   <div class="col-12">
		<div class="card">
			<div class="card-header">
			   <div class="row">
					<div class="col-md-8">  <a data-toggle="collapse" href="#collapse<?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'section'); ?>" class="topic"> <?php echo $topiciconlock; ?> <i class="fa fa-list-alt" aria-hidden="true"></i> <?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'name'); ?></a></div>
					<div class="col-md-4 progressinfo<?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'status'); ?> text-right "> <?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'progresspercentinfo'); ?> <i class="<?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'statusicon'); ?> " ></i></div>
			   </div> 
			</div>   
			<div class="card-body">			
			   <div class="list-group-topics">
				<ul class="list-group1">
					<?php 
					$bmvlactivities=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'activities');
					foreach ($bmvlactivities as $itemactivity){
						$atname=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'name');
						$auservisible=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'uservisible');
						$aavailable=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'available');
						$atlocked=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'locked');
						$aturl=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'url');
						$atstatus=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'status');
						$aturlicon=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'urlicon');
						$atstatuslabel=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'statuslabel');
						$atstatusicon=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'statusicon');
						if($aavailable){
						?>
					
					<li class="list-group-item">
					   <div   class="row">
						<?php if($atlocked  ){ ?>
							<div class="col-md-8"><img src ="<?php echo $aturlicon; ?>">&nbsp;&nbsp;<?php echo $atname; ?></div> 
						<?php }else{ ?>
							<div class="col-md-8"><a href="<?php echo $aturl; ?>"><img src ="<?php echo $aturlicon; ?>">&nbsp;&nbsp;<?php echo $atname; ?> </a></div> 
						<?php } ?>
						<div class="col-md-4 progressinfo text-right <?php echo $atstatus; ?>">
						<?php echo $atstatuslabel; ?><i class="<?php echo $atstatusicon; ?>" aria-hidden="true"></i> 
						</div>
					  </div>	
					 </li> 
						<?php }}?> <!--end foreach activities-->
					
				</ul>
			<br /> </div>	
			</div>  
		</div><br />
	 </div <!--end col-12-->
	<?php }?> <!--end if whow for first topic -->
	<?php }?> <!--end foreach topics-->
	</div> 
	 
	
</div>