
	<div class="panel-group">
	<?php 
	 	$count=0;
		foreach ($badiumfview->getData()->topics as $itemtopic){
		$count++;
		 $bmsummary=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'summary');
		 $bmsummary=badiumview_addtext_image($bmsummary,$badiumfview);
		 
		 $tuservisible=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'uservisible');
		 $tvisible=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'visible');
		
		?>
	
		<div class="panel panel-default">
			<div class="panel-heading row">
				<div class="col-md-8 panel-title">  <a data-toggle="collapse" href="#collapse<?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'section'); ?>" class="topic"> <?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'name'); ?></a></div>
				<div class="col-md-4 progressinfo <?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'status'); ?>"> <?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'progresspercentinfo'); ?> <i class="<?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'statusicon'); ?>" aria-hidden="true"></i></div>
				
			</div>    
			<div id="collapse<?php echo $badiumfview->getUtildata()->getVaueOfArray($itemtopic,'section'); ?>" class="panel-collapse collapse in"> 
			   <div class="list-group-topics">
			   <?php echo $bmsummary; ?>
				<ul class="list-group">
					<?php 
					$bmvlactivities=$badiumfview->getUtildata()->getVaueOfArray($itemtopic,'activities');
					foreach ($bmvlactivities as $itemactivity){
						$atname=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'name');
						$auservisible=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'uservisible');
						$aavailable=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'available');
						$atlocked=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'locked');
						$aturl=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'url');
						$atstatus=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'status');
						$aturlicon=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'urlicon');
						$atstatuslabel=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'statuslabel');
						$atstatusicon=$badiumfview->getUtildata()->getVaueOfArray($itemactivity,'statusicon');
						if($aavailable){
						?>
					
					<li class="list-group-item">
					   <div   class="row">
						<?php if($atlocked || !$tuservisible){ ?>
							<div class="col-md-8"><img src ="<?php echo $aturlicon; ?>">&nbsp;&nbsp;<?php echo $atname; ?></div> 
						<?php }else{ ?>
							<div class="col-md-8"><a href="<?php echo $aturl; ?>"><img src ="<?php echo $aturlicon; ?>">&nbsp;&nbsp;<?php echo $atname; ?> </a></div> 
						<?php } ?>
						<div class="col-md-4 progressinfo <?php echo $atstatus; ?>">
						<?php echo $atstatuslabel; ?><i class="<?php echo $atstatusicon; ?>" aria-hidden="true"></i> 
						</div>
					   </div>
					 </li> 
						<?php }}?> <!--end foreach activities-->
					
				</ul>
			<br /> </div>	
			</div> 
		</div>

	<?php if($count==1){break;}}?> <!--end foreach topics-->
	</div> 
	
<hr />
<?php 
function badiumview_addtext_image($text,$badiumfview){ 
	global $COURSE;
	global $DB;
	global $CFG;
	$courseid=$COURSE->id;
	if(empty($courseid)){return $text;}
	$sql = "SELECT id FROM {$CFG->prefix}context WHERE contextlevel=50 AND instanceid=$courseid";
	$row = $DB->get_record_sql($sql);
	if(empty($row)){return $text;}
	$contextid=$row->id;
	if(empty($contextid)){return $text;}
	$sessionid=$badiumfview->getUtildata()->getVaueOfArray($badiumfview->getData()->topics,'0.id',true);
	if(empty($sessionid)){return $sessionid;}
	$drwww="$CFG->wwwroot/pluginfile.php/$contextid/course/section/$sessionid";
	$text=str_replace("@@PLUGINFILE@@",$drwww,$text);
	return $text; 
}
?>
