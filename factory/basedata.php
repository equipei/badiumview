<?php
require_once("$CFG->dirroot/theme/badiumview/locallib/baselib.php");
class theme_badiumview_factory_basedata extends theme_badiumview_baselib
{
	private $data;
	function __construct($bparam = null)
	{
		parent::__construct($bparam);
		$this->data = new stdClass();
	}

	public function getData()
	{
		return $this->data;
	}

	public function setData($data)
	{
		$this->data = $data;
	}

	public function initTemplate()
	{
		global $CFG;
		$dirpath = $CFG->badiumview->packagepath;

		if (!isset($CFG->badiumview->param->mustache->data)) {
			return null;
		}
		if (!is_dir("$dirpath/templates")) {
			return null;
		}
		if (!isset($CFG->badiumview->param->mustache->template)) {
			return null;
		}
		$template = $CFG->badiumview->param->mustache->template;
		if (!file_exists("$dirpath/templates/$template.mustache")) {
			return null;
		}
		$m = new Mustache_Engine(array('loader' => new Mustache_Loader_FilesystemLoader("$dirpath/templates")));
		$this->getData()->template = $m->render($template, $CFG->badiumview->param->mustache->data);
	}
}
