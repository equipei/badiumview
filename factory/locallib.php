<?php
class theme_badiumview_factory_locallib
{
	public function getContent($type, $template, $param = null)
	{
		global $CFG;
		//$BFVCFG=new  stdClass();
		$template = preg_replace('/' . $type . '/', '', $template, 1);
		$type = preg_replace('/factory/', '', $type, 1);

		$content = "";
		//$BFVCFG->dirroot=$CFG->dirroot;
		//$BFVCFG->param=$param;
		$dirpath1 =  $CFG->dataroot . "/theme/badiumview/factory/$type/$template";
		$dirpath2 =  $CFG->dirroot . "/theme/badiumview/factory/$type/$template";

		$result = new  stdClass();
		$badiumview = new  stdClass();
		$badiumview->param = $param;

		$layout = "default";
		if ($type == 'theme') {
			$layout = "themecoretemplate";
			if (isset($param->layout)) {
				$layout = $param->layout;
			}
		}

		$dirpath = null;

		if (file_exists($dirpath1)) {
			$dirpath = $dirpath1;
			$badiumview->param->mustache->data['webview'] = "$CFG->wwwroot/theme/badiumview/factory/webview.php/1/$type/$template/";
			$badiumview->urlwebview = "$CFG->wwwroot/theme/badiumview/factory/webview.php/1/$type/$template";
		} else if (file_exists($dirpath2)) {
			$dirpath = $dirpath2;
			$badiumview->param->mustache->data['webview'] = "$CFG->wwwroot/theme/badiumview/factory/webview.php/0/$type/$template/";
			$badiumview->urlwebview = "$CFG->wwwroot/theme/badiumview/factory/webview.php/0/$type/$template";
		}
		$result->urlwebview = $badiumview->urlwebview;
		$badiumview->packagepath = $dirpath;
		$CFG->badiumview = $badiumview;

		if ($layout == "default") {
			$file = $dirpath . '/index.php';
			$result = $this->getFlieContent($file);
		} else if ($layout == "themecoretemplate") {
			//header
			$CFG->badiumview->param->mustache->template = 'header';
			$file = $dirpath . '/header.php';
			$result->header = $this->getFlieContent($file);

			//footer
			$CFG->badiumview->param->mustache->template = 'footer';
			$file = $dirpath . '/footer.php';
			$result->footer = $this->getFlieContent($file);

			//css
			if (file_exists("$dirpath/style.css")) {
				$result->cssurl = $CFG->badiumview->urlwebview . "/style.css";
			} else $result->cssurl = null;
		} else if ($layout == "themelogintemplate") {
			//header
			$CFG->badiumview->param->mustache->template = 'header';
			$file = $dirpath . '/login/header.php';
			if (file_exists($file)) {
				$result->header = $this->getFlieContent($file);
			}
			/*else {
				$file=$dirpath.'/header.php';
				$result->header=$this->getFlieContent($file);
			}*/

			//loginform
			$CFG->badiumview->param->mustache->template = 'loginform';
			$file = $dirpath . '/login/form.php';

			if (file_exists($file)) {
				$result->form = $this->getFlieContent($file);
			} else {
				$file = $dirpath . '/loginform.php';
				$result->form = $this->getFlieContent($file);
			}

			//footer
			$CFG->badiumview->param->mustache->template = 'footer';
			$file = $dirpath . '/login/footer.php';
			if (file_exists($file)) {
				$result->footer = $this->getFlieContent($file);
			}
			/*else {
				$file=$dirpath.'/footer.php';
				$result->footer=$this->getFlieContent($file);
			}*/

			//css
			if (file_exists("$dirpath/style.css")) {
				$result->cssurl = $CFG->badiumview->urlwebview . "/style.css";
			} else $result->cssurl = null;
		}

		$CFG->badiumview = null;
		return $result;
	}

	public function getFlieContent($file)
	{
		global $CFG;

		if (!file_exists($file)) {
			return null;
		}
		$result = new  stdClass();
		$content = null;
		ob_start();
		include_once($file);
		$result->content = ob_get_contents();
		$result->data = $badiumfview;
		ob_end_clean();
		return $result;
	}
	/*
    public function get_categories($type) {
        global $CFG,$DB;
         $sql="SELECT id,name,description FROM {$CFG->prefix}course_categories WHERE idnumber LIKE '".$type."%'";
		
        $rows = $DB->get_records_sql($sql);
	     return $rows;
    }

    public function get_subelement($catid) {
        $catlist=$this->get_subcategories($catid);
        $courselist=$this->get_courses($catid);

        $newlist=array();
        if(is_array($catlist)){
            foreach ($catlist as $row) {
                $row->type='category';
                array_push( $newlist,$row);
            }
        }
        if(is_array($courselist)){
            foreach ($courselist as $row) {
                $row->type='course';
                array_push( $newlist,$row);
            }
        }
        return $newlist;
    }
	public function get_subcategories($catid) {
        global $CFG,$DB;
         $sql="SELECT id,name,description FROM {$CFG->prefix}course_categories WHERE parent=$catid ORDER BY sortorder ";
       $rows = $DB->get_records_sql($sql);
      
	     return $rows;
    }
	public function get_courses($catid) {
        global $CFG,$DB;
         $sql="SELECT id,fullname AS name,summary AS description FROM {$CFG->prefix}course WHERE category=$catid  ORDER BY sortorder ";
       $rows = $DB->get_records_sql($sql);
      
	     return $rows;
    }*/
	/**
	 * Clean uri.
	 *
	 * @return string
	 */
	function clean_uri()
	{
		global $CFG;
		$uri  = $_SERVER['REQUEST_URI'];
		$host = $CFG->wwwroot;
		$p    = explode("//", $host);
		if (!isset($p[1])) {
			return $uri;
		}
		$shost = explode("/", $p[1]);
		$suri  = explode("/", $uri);

		$cont   = 0;
		$newuri = "";
		foreach ($suri as $key => $value) {
			$phost = null;
			if (isset($shost[$key])) {
				$phost = $shost[$key];
			}
			if ($key > 0) {
				if (empty($value)) {
					$newuri .= '/';
					$cont++;
				} else if ($phost != $value) {
					if ($newuri != '/') {
						$newuri .= '/' . $value;
						$cont++;
					}
				}
			}
		}
		if ($cont == 0) {
			return $uri;
		}
		return $newuri;
	}

	/**
	 * Check if user is is teachear or admin in course.
	 *
	 * @param $courseid
	 * @return bool
	 * @throws coding_exception
	 * @throws dml_exception
	 */
	function is_teachear_or_admin($courseid)
	{
		if (has_capability('moodle/site:config', context_system::instance())) {
			return true;
		} else if (has_capability('report/courseoverview:view', context_course::instance($courseid))) {
			return true;
		}
		return false;
	}


	/**
	 * Check if user is admin
	 * @throws coding_exception
	 * @throws dml_exception
	 */
	function is_admin()
	{
		if (has_capability('moodle/site:config', context_system::instance())) {
			return true;
		}
		return false;
	}
}
