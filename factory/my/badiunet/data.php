<?php
require_once("$CFG->dirroot/theme/badiumview/factory/my/locallib.php");
class theme_badiumview_factory_my_badiunet_data extends  theme_badiumview_factory_my_locallib {
 
 function __construct() {
		parent::__construct();
		$this->init();
		$this->check();
    }
	
	public function check() { 
		global $CFG;
		if($this->getData()->istudent){
			$url    = "$CFG->wwwroot/local/badiunet/fcservice/index.php?_key=badiu.ams.my.studentoffer.dashboard";
			redirect($url);
		}else{
			$url    = "$CFG->wwwroot/theme/badiumview/factory/my/template1/view.php";
			redirect($url);
		}
	}	
 
}	
?>
