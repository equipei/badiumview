<style>
    .backcover {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 127px;
        opacity: 0.85;

        background: #eb01a5;
        background-image: url("images/capa.png");
        background-image: url("images/capa.png"), linear-gradient(88.34deg, #2B588B 0%, #B0D5F0 99.98%);
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .main-title {
        font-family: "Open Sans";
        font-weight: bold;
        font-size: 32px;
        line-height: 44px;
        color: #FFFFFF;
        text-shadow: 0px 2px 12px rgba(0, 0, 0, 0.3);
    }

    .page-title {
        font-family: "Open Sans";
        font-weight: 700;
        font-size: 32px;
        color: #404F5A;
        text-align: center;
    }

    .has-search {
        position: relative;
    }

    .has-search .form-control {
        border: 0.5px solid #C0C9CF;
        box-shadow: 0px 12.3517px 32.9379px rgba(0, 0, 0, 0.04);
        border-radius: 6px;
        padding: 0 32px 0 16px;

        font-family: "Open Sans";
        font-style: normal;
        font-weight: normal;
        font-size: 16px;
        line-height: 22px;
        color: #7D8890;
    }

    .has-search .form-control-feedback {
        position: absolute;
        top: 0;
        right: 0;
        z-index: 2;
        margin: 6px 10px;
    }

    .has-search .form-control-feedback .image-search {
        width: 13px;
        height: 13px;
    }

    .container-category {
        width: 395px;
        height: 102px;
        background: #FFFFFF;
        border: 1px solid rgba(192, 201, 207, 0.6);
        border-radius: 8px;
        display: flex;
        align-items: center;
        margin: 12px;
    }

    .category-title {
        font-family: "Open Sans";
        font-weight: 400;
        font-size: 20px;
        color: #7D8890;
    }

    .category-total {
        font-family: "Open Sans";
        font-style: normal;
        font-weight: bold;
        font-size: 12px;
    }

    .color-1 {
        color: #C895B6;
    }

    .color-2 {
        color: #6EA3AC;
    }

    .color-3 {
        color: #ABBC82;
    }

    .color-4 {
        color: #D9A779;
    }

    .color-5 {
        color: #7BA1D6;
    }





    /* GRID CARDS */

    .grid-cards {
        display: grid;
        grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
        grid-column-gap: 24px;
        grid-row-gap: 24px;
    }

    .card-content {
        background: #FFFFFF;
        border: 1px solid #C0C9CF;
        box-shadow: 0px 24px 64px rgba(0, 0, 0, 0.04);
        border-radius: 8px;
        height: 100% !important;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        padding-bottom: 1.25rem;
        position: relative;
    }

    .card-title {
        font-family: "Open Sans";
        font-weight: 700;
        font-size: 16px;
        color: #404F5A;
    }

    .card-highlight {
        display: flex;
        align-items: center;
        padding-left: 1.25rem;
        height: 25px;
        font-family: "Open Sans";
        font-weight: 600;
        font-size: 9px;
        text-transform: uppercase;
        color: #FFFFFF;
    }

    .card-subtitle {
        font-family: "Open Sans";
        font-weight: 600;
        font-size: 9px;
        color: #7D8890;
    }

    .card-bg-color-1 {
        background-color: #7BA1D6;
    }

    .card-bg-color-2 {
        background-color: #C895B6;
    }

    .card-bg-color-3 {
        background-color: #ABBC82;
    }

    .card-bg-color-4 {
        background-color: #D9A779;
    }

    .card-text-color-1 {
        color: #7BA1D6;
    }

    .card-text-color-2 {
        color: #C895B6;
    }

    .card-text-color-3 {
        color: #ABBC82;
    }

    .card-text-color-4 {
        color: #D9A779;
    }

    .btn-card {
        background: #3168A6;
        box-shadow: 2px 4px 8px rgba(20, 46, 82, 0.2);
        border-radius: 4px;
        border: none;
        padding: 8px 12px;

        color: #fff;
        font-weight: 400;
        font-size: 12px;
        line-height: 18px;

        opacity: 1;
        transition: all 0.2s;
    }

    .btn-card:hover {
        color: #ffffff;
        opacity: 0.6;
    }

    .btn-card-info {
        background: #FFFFFF;
        border: 1px solid #3168A6;
        box-sizing: border-box;
        box-shadow: 0px 2px 4px rgba(20, 46, 82, 0.2);
        border-radius: 4px;
        padding: 4px 8px;
        opacity: 1;
        transition: all 0.2s;
        color: #3168A6;
    }

    .btn-card-info:hover {
        color: #3168A6;
        opacity: 0.6;
    }

    .text-status {
        font-family: "Open Sans";
        font-weight: 700;
        font-size: 12px;
    }

    .card-img {
        position: relative;
        overflow: hidden;
        border-top-left-radius: calc(0.5rem - 1px);
        border-top-right-radius: calc(0.5rem - 1px);

        border-bottom-left-radius: unset;
        border-bottom-right-radius: unset;

        z-index: 1;
    }

    .card-img .card-image {
        width: 100%;
        object-fit: cover;
        transition: transform .3s ease-in-out;
    }

    .card-content:hover img {
        transform: scale(1.05);
    }

    /* Small devices (landscape phones, 576px and up) */
    @media (min-width: 576px) {
        .card-img .card-image {
            height: 19vw;
        }
    }

    /* Medium devices (tablets, 768px and up) */
    @media (min-width: 768px) {
        .card-img .card-image {
            height: 16vw;
        }
    }

    /* Large devices (desktops, 992px and up) */
    @media (min-width: 992px) {
        .card-img .card-image {
            height: 11vw;
        }
    }

    /* Extra large devices (large desktops, 1200px and up) */
    @media (min-width: 992px) {
        .card-img .card-image {
            height: 11vw;
        }
    }

    .card-desc {
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        padding: 1.25rem 1.25rem 0 1.25rem;
    }

    .card-desc h3 {
        color: #000000;
        font-weight: 600;
        font-size: 1.5em;
        line-height: 1.3em;
        margin-top: 0;
        margin-bottom: 5px;
        padding: 0;
    }

    .card-desc p {
        font-family: "Open Sans";
        font-weight: 400;
        font-size: 12px;
        color: #7D8890;

        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;

        line-clamp: 6;
        -webkit-line-clamp: 6;

        -webkit-box-orient: vertical;
        box-orient: vertical;
    }

    .card-options {
        padding: 1.25rem 1.25rem 0 1.25rem;
    }

    .text-subtext {
        font-weight: 700;
        font-size: 0.5625rem;
    }

    .text-progress {
        font-family: "Open Sans";
        font-weight: 400;
        font-size: 11px;
        color: #7D8890;
    }

    .text-prevision {
        font-family: "Open Sans";
        font-style: italic;
        font-weight: 400;
        font-size: 11px;
        color: #7D8890;
    }

    .progress {
        background-color: #C0C9CF !important;
        border: none;
    }

    .bg-progress {
        background-color: #7BA1D6 !important;
    }
</style>

<div class="backcover">
    <h1 class="main-title">Catálogo de cursos</h1>
</div>

<h2 class="page-title mt-5 mb-3">Qual curso você está procurando?</h2>

<p class="text-center">A EV.G classifica os cursos por temáticas. Assim, você pode se especializar em uma área e fazer a diferença na sua atuação.</p>

<div class="form-group has-search">
    <input type="text" class="form-control" placeholder="O que você procura?">
    <!--span class="fa fa-search form-control-feedback"></span-->
    <div class="form-control-feedback">
        <img class="image-search" src="images/search.svg" alt="">
    </div>
</div>

<h3 class="page-title mt-5 mb-3">Categorias</h3>

<div class="d-flex flex-wrap align-items-center justify-content-center">

    <div class="container-category">
        <div class="category-img">
            <img src="images/img1.png" alt="">
        </div>
        <div class="category-data py-2 px-4">
            <div class="category-title">Arbitragem</div>
            <div class="category-total color-1">14 CURSOS</div>
        </div>
    </div>

    <div class="container-category">
        <div class="category-img">
            <img src="images/img2.png" alt="">
        </div>
        <div class="category-data py-2 px-4">
            <div class="category-title">Desenvolvimento pessoal e profissional</div>
            <div class="category-total color-2">14 CURSOS</div>
        </div>
    </div>

    <div class="container-category">
        <div class="category-img">
            <img src="images/img3.png" alt="">
        </div>
        <div class="category-data py-2 px-4">
            <div class="category-title">Educação e docência</div>
            <div class="category-total color-3">14 CURSOS</div>
        </div>
    </div>

    <div class="container-category">
        <div class="category-img">
            <img src="images/img4.png" alt="">
        </div>
        <div class="category-data py-2 px-4">
            <div class="category-title">Justiça restaurativa</div>
            <div class="category-total color-4">14 CURSOS</div>
        </div>
    </div>

    <div class="container-category">
        <div class="category-img">
            <img src="images/img5.png" alt="">
        </div>
        <div class="category-data py-2 px-4">
            <div class="category-title">Mediação, conciliação e negociação</div>
            <div class="category-total color-5">14 CURSOS</div>
        </div>
    </div>
</div>

<h3 class="page-title mt-5">Conheça nossos cursos</h3>

<div class="grid-cards my-5">

    <?php
    // EXEMPLOS
    $card1 = new stdClass;
    $card1->image = "images/img-card.png";
    $card1->destaque = "MEDIAÇÃO, CONCILIAÇÃO E NEGOCIAÇÃO";
    $card1->titulo = "Fundamentos da lei geral de proteção de dados";
    $card1->texto = "Descrição do curso convallis et iaculis porttitor, accumsan ac purus. Curabitur laoreet aliquet accumsan.";
    $card1->cargaHoraria = "30";
    $card1->certificador = "ENAPRES";
    $card1->stAndamento = true;
    $card1->progresso = "60";
    $card1->previsao = "20/08/2021";
    $card1->status = 1;

    $card2 = new stdClass;
    $card2->image = "images/img-card.png";
    $card2->destaque = "ARBITRAGEM";
    $card2->titulo = "Fundamentos da lei geral de proteção de dados";
    $card2->texto = "Descrição do curso convallis et iaculis porttitor, accumsan ac purus. Curabitur laoreet aliquet accumsan.";
    $card2->cargaHoraria = "30";
    $card2->certificador = "ENAPRES";
    $card2->stAndamento = false;
    $card2->progresso = "";
    $card2->previsao = "";
    $card2->status = 3;

    $card3 = new stdClass;
    $card3->image = "images/img-card.png";
    $card3->destaque = "EDUCAÇÃO E DOCÊNCIA";
    $card3->titulo = "Fundamentos da lei geral de proteção de dados";
    $card3->texto = "Descrição do curso convallis et iaculis porttitor, accumsan ac purus. Curabitur laoreet aliquet accumsan.";
    $card3->cargaHoraria = "30";
    $card3->certificador = "ENAPRES";
    $card3->stAndamento = false;
    $card3->progresso = "";
    $card3->previsao = "";
    $card3->status = 2;

    $card4 = new stdClass;
    $card4->image = "images/img-card.png";
    $card4->destaque = "JUSTIÇA RESTAURATIVA";
    $card4->titulo = "Fundamentos da lei geral de proteção de dados";
    $card4->texto = "Descrição do curso convallis et iaculis porttitor, accumsan ac purus. Curabitur laoreet aliquet accumsan.";
    $card4->cargaHoraria = "30";
    $card4->certificador = "ENAPRES";
    $card4->stAndamento = false;
    $card4->progresso = "";
    $card4->previsao = "";
    $card4->status = 2;

    $arrCards = [1 => $card1, 2 => $card2, 3 => $card3, 4 => $card4];

    foreach ($arrCards as $key => $value) {
    ?>
        <div class="card-content">
            <div>
                <div class="card-img">
                    <img src="<?php echo $value->image; ?>" class="card-image" alt="">
                </div>
                <div class="card-highlight card-bg-color-<?php echo $key; ?>">
                    <?php echo $value->destaque; ?>
                </div>

                <div class="card-desc">
                    <div class="d-flex align-items-baseline mb-1">
                        <h5 class="card-title mb-0">
                            <?php echo $value->titulo; ?>
                        </h5>
                    </div>
                    <p class="mt-1">
                        <?php echo $value->texto; ?>
                    </p>
                </div>
            </div>

            <div class="card-options">
                <div class="d-flex align-items-center mb-3">
                    <div class="d-flex flex-column w-50">
                        <span class="card-subtitle">CARGA HORÁRIA</span>
                        <span class="d-flex align-items-center text-subtext card-text-color-<?php echo $key; ?>">
                            <img src="images/clock.svg" class="mr-1"> <?php echo $value->cargaHoraria; ?>h
                        </span>
                    </div>
                    <div class="d-flex flex-column w-50">
                        <span class="card-subtitle">CERTIFICADOR</span>
                        <span class="text-subtext card-text-color-<?php echo $key; ?>"><?php echo $value->certificador; ?></span>
                    </div>
                </div>

                <?php if ($value->stAndamento) : ?>
                    <div>
                        <p class="text-progress color-1 mb-1">
                            <?php echo $value->progresso; ?>% concluído
                        </p>
                        <div class="progress mb-1" style="height: 4px;">
                            <div class="progress-bar bg-progress" role="progressbar" style="width: <?php echo $value->progresso; ?>%" aria-valuenow="<?php echo $value->progresso; ?>" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="d-flex align-items-center justify-content-between">
                            <p class="text-prevision mb-0">Previsão de conclusão: <?php echo $value->previsao; ?></p>
                        </div>
                    </div>

                <?php else : ?>
                    <div class="d-flex align-items-center justify-content-between">
                        <?php if ($value->status === 3) : ?>
                            <a href="#" class="btn btn-card">Emitir certificado</a>
                        <?php else : ?>
                            <a href="#" class="btn btn-card">Nova inscrição</a>
                        <?php endif; ?>

                        <button class="btn btn-card-info">
                            <i class="fa fa-info-circle"></i>
                        </button>

                        <?php if ($value->status === 3) : ?>
                            <span class="text-status card-text-color-<?php echo $key; ?>"><i class="far fa-check-circle"></i> Concluído</span>
                        <?php else : ?>
                            <span class="text-status card-text-color-<?php echo $key; ?>"><i class="far fa-times-circle"></i> Cancelado</span>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php
    }
    ?>
</div>