<style>
    .backcover {
        display: flex;
        align-items: center;
        justify-content: center;
        height: 127px;
        opacity: 0.85;

        background: #eb01a5;
        background-image: url("images/capa.png");
        background-image: url("images/capa.png"), linear-gradient(88.34deg, #2B588B 0%, #B0D5F0 99.98%);
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    .main-title {
        font-family: "Open Sans";
        font-weight: bold;
        font-size: 32px;
        line-height: 44px;
        color: #FFFFFF;
        text-shadow: 0px 2px 12px rgba(0, 0, 0, 0.3);
    }

    .page-title {
        font-family: "Open Sans";
        font-weight: 700;
        font-size: 32px;
        color: #404F5A;
        text-align: center;
    }

    .container-contact {
        background-color: #F8F8F8;
        padding: 20px 0;
    }

    .text-page-contacts {
        font-family: "Open Sans";
        font-weight: 400;
        font-size: 16px;
        line-height: 24px;
        color: #404F5A;
    }

    .accordion>.card>.card-header {
        margin-bottom: 0;
    }

    .accordion .card {
        background: #FFFFFF;
        margin-bottom: 10px;
        border: none;
    }

    .accordion .card-header {
        padding: .75rem 1.25rem;
        margin-bottom: 0;
        background: #FFFFFF;
        border: 1px solid #C0C9CF;
        border-radius: 8px;
        display: flex;
        align-items: center;
    }

    .accordion .card-header button {
        font-family: Open Sans;
        font-weight: 400;
        font-size: 16px;
        line-height: 24px;

        /* Enapres/Cinza/Escuro */
        color: #404F5A;
    }

    .accordion .card-header button:after {
        float: right;
        font-family: "Font Awesome\ 5 Free";
        font-weight: 900;
        font-size: 16px;
        content: "\f068";
        /* Enapres/Azul/Escuro2 */
        color: #0F3764;
    }

    .accordion .card-header button.collapsed:after {
        content: "\f067";
        /* Enapres/Verde/Escuro */
        color: #6EA3AC;
    }

    .accordion .card-body {
        margin: 10px 0;
        background: #F0F7F3;
        border-radius: 8px;
    }

    .card-boyd-title {
        font-family: "Open Sans";
        font-weight: 700;
        font-size: 16px;
        line-height: 24px;
        /* Enapres/Azul/Escuro2 */
        color: #0F3764;
    }

    .grid-card-phones {
        display: grid;
        grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
        grid-column-gap: 28px;
        grid-row-gap: 28px;
        padding: 48px 20px 96px 20px;
    }

    .card-phone {
        display: flex;
        flex-direction: column;
        background: #FFFFFF;
        box-shadow: 0px 2px 4px rgba(20, 46, 82, 0.1);
        border-radius: 8px;
        padding: 40px 20px;
    }

    .title-card-phone {
        font-family: "Open Sans";
        font-weight: 700;
        font-size: 18px;
        line-height: 24px;
        text-align: center;
        color: #404F5A;
    }

    .text-card-phone {
        font-family: "Open Sans";
        font-weight: 400;
        font-size: 18px;
        line-height: 24px;
        text-align: center;
        color: #404F5A;
    }
</style>

<div class="backcover">
    <h1 class="main-title">Perguntas Frequentes</h1>
</div>

<div class="accordion my-5" id="accordionExample">
    <div class="card">
        <div class="card-header" id="headingOne">
            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Os cursos a distância da EV.G são gratuitos? A quem se destinam?
            </button>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <h3 class="card-boyd-title mb-3">Os cursos a distância da EV.G são gratuitos? A quem se destinam?</h3>
                <p>Todos os cursos online da Escola Virtual.Gov (EV.G) são sem tutoria e gratuitos.</p>
                <p>No geral, são cursos abertos, não apresentam limite de vagas e podem ser cursados a qualquer momento, por qualquer pessoa, seja ela servidora pública ou não.</p>
                <p>Nos cursos abertos, você poderá se inscrever em quantos cursos quiser, a qualquer tempo, inclusive no mesmo período, desde que tenha disponibilidade para cursá-los.</p>
                <p>Atenção: reforçamos a importância de verificar seu tempo disponível para conseguir acompanhar os cursos que você se inscrever.</p>
                <p>Temos casos de cursos exclusivos para um público-alvo específico, ou seja, somente as pessoas que fazem parte daquele público-alvo poderão realizar o curso. Nesses casos, os critérios estão discriminados na seção Público-alvo.</p>
                <p>Todos os nossos cursos são certificados, desde que você consiga atingir os critérios informados no “Guia do Participante”.</p>
                <p>A certificação e a emissão do certificado também são gratuitas.</p>
                <p>O certificado é disponibilizado de forma digital (https://www.escolavirtual.gov.br/aluno/certificados).</p>
                <p>Não emitimos certificados impressos.</p>
                <p>Acesse o Catálogo de Cursos, escolha algum curso de seu interesse e comece agora mesmo a cursá-lo!</p>
            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-header" id="headingTwo">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Os cursos a distância da Escola Virtual.Gov são reconhecidos pelo MEC?
            </button>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="card-body">
                Some placeholder content for the second accordion panel. This panel is hidden by default.
            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-header" id="headingThree">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Por que o curso que tenho interesse está com a inscrição fechada (indisponível)?
            </button>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
                And lastly, the placeholder content for the third and final accordion panel. This panel is hidden by default.
            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-header" id="headingThree">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Qual é o prazo de realização dos cursos? O que é o período de disponibilidade do curso?
            </button>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aperiam alias tenetur, eos dolor dolores ex! Dicta, repellat doloremque accusantium quos commodi hic eaque deleniti numquam dignissimos unde veniam fugiat totam!
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header" id="headingThree">
            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Não tenho mais acesso ao e-mail registrado em meu cadastro antigo.
            </button>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="card-body">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aperiam alias tenetur, eos dolor dolores ex! Dicta, repellat doloremque accusantium quos commodi hic eaque deleniti numquam dignissimos unde veniam fugiat totam!
            </div>
        </div>
    </div>
</div>


<div class="container-contact">
    <h2 class="page-title mt-5 mb-3">Fale Conosco</h2>

    <p class="text-page-contacts text-center">
        Subtítulo da seção ipsum dolor sit amet, consectetur adipiscing elit. Nam sodales erat non feugiat fringilla. Etiam elementum leo eget luctus imperdiet.
    </p>

    <div class="grid-card-phones">
        <div class="card-phone">
            <img src="images/phone.svg" alt="Contato">
            <span class="title-card-phone mb-2">Telefone</span>
            <span class="text-card-phone">99999-9999</span>
        </div>

        <div class="card-phone">
            <img src="images/phone.svg" alt="Contato">
            <span class="title-card-phone mb-2">Telefone</span>
            <span class="text-card-phone">99999-9999</span>
        </div>

        <div class="card-phone">
            <img src="images/phone.svg" alt="Contato">
            <span class="title-card-phone mb-2">Telefone</span>
            <span class="text-card-phone">99999-9999</span>
        </div>
    </div>
</div>