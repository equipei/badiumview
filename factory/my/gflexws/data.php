<?php
require_once("$CFG->dirroot/theme/badiumview/locallib/badiunet/locallib.php");
require_once("$CFG->dirroot/theme/badiumview/factory/basedata.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/course/rolelib.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/pluginconfig.php");
class theme_badiumview_factory_my_gflexws_data extends  theme_badiumview_factory_basedata {
 
 function __construct() {
	 parent::__construct();
		$this->init();
    }
	 
 function init() { 
	global $USER;
	$userid=$USER->id;
	
	$rolelib=new theme_badiumview_course_rolelib();
		$this->getData()->isteachear=false;
		$this->getData()->istudent=true;
		$isteachear=$rolelib->hasTeachearProfile($userid);
		if($isteachear){
			$this->getData()->isteachear=true;
			$this->getData()->istudent=false;
		}
		$lm=optional_param('lm', NULL,PARAM_INT);
		//temp
		if($lm==1){
			 $this->getData()->isteachear=true;
			 $this->getData()->istudent=false;
		}else if($lm==2){
			 $this->getData()->isteachear=false;
			 $this->getData()->istudent=true;
		}
		 
	$this->getData()->pagemenu=optional_param('_badiupagemenugflex', NULL,PARAM_TEXT);
	if(empty($this->getData()->pagemenu)){$this->getData()->pagemenu='amscurrentdiscipline';};
	
	$this->intMessageshowinhead();
	if($this->getData()->pagemenu=='amstododiscipline' || $this->getData()->pagemenu=='amscurriculum'){
		$result=$this->ws_historico_escolar();
		$result=$this->filterDisciplineShow($result);
		$this->getData()->historico=$result;
		$this->getData()->pagetitle=$this->get_title();
		
		$this->getData()->progressinfo="";
		$this->getData()->progresslabel="";
		if($this->getData()->pagemenu=='amscurriculum'){
			$progressinfo=$this->count_completed() ;
			$this->getData()->progressinfo=$progressinfo;
			$progresslabel=$this->progress_bar($progressinfo);
			$this->getData()->progresslabel=$progresslabel;
			
		}
		
	
		
	 
		
	}else if($this->getData()->pagemenu=='amscurrentdiscipline'){
		
		
	}
	
	 
	//$this->getData()->pagetitle='Minha Trilha';
	
}	

 //custom message
 function intMessageshowinhead() {
		$fparam=array();
		$fparam=array('dtype'=>'system','instanceid'=>1,'name'=>'course.head');
		$factorymymessageshowinhead=$this->getMconfig()->get_value($fparam);
		if(!empty($factorymymessageshowinhead->value)){$factorymymessageshowinhead=$factorymymessageshowinhead->value;}		
		if(empty($factorymymessageshowinhead)){
			$themeconfig=new theme_badiumview_pluginconfig();
			$factorymymessageshowinhead=$themeconfig->getValue('factorymymessageshowinhead');
		}
		$this->getData()->messageshowinhead=$factorymymessageshowinhead;
 }
function ws_historico_escolar() {
		$matricula=optional_param('matricula', NULL,PARAM_INT);
		if(empty($matricula)){
			global $USER;
			$matricula=$USER->username;
			if(empty($matricula)){
				$msg="";
				$msg.='<div class="alert alert-danger" role="alert">';
				$msg.="Está faltando o parâmetro da matrícula";
				$msg.='</div>';
				echo $msg;
				return null;
			}
			
			
		}
        $param=array();
		$fparam=array('dtype'=>'system','instanceid'=>1,'name'=>'gflex.webservice.token');
		$wstoken=$this->getMconfig()->get_value($fparam);
		$wstoken=$wstoken->value;
		$wsfunction = 'ws_historico_escolar';
		$matricula=         $matricula;
		$modelo          =8;
		$query="?wstoken=$wstoken&wsfunction=$wsfunction&matricula=$matricula&modelo=$modelo";
		
        $result=$this->request($query);
		return  $result;
    }
	
	/*function ws_listar_matriculas_aluno() {
		$cpf=optional_param('cpf', NULL,PARAM_TEXT);
	  //if(empty($cpf)){echo "passe parametro cpf";exit;}
        $param=array();
		$wstoken="******";
        $wsfunction = 'ws_listar_matriculas_aluno';
		$cpf=         $cpf;
		$query="?wstoken=$wstoken&wsfunction=$wsfunction&cpf=$cpf";
		
        $result=$this->request($query);
		return  $result;
    }*/
	
  /**
     * Convert dates in human dates.
     *
     * @param $data
     * @param bool $outarray
     * @return mixed
     */
    private  function convertData($data,$outarray=true) {
        $max_int_length       = strlen((string) PHP_INT_MAX) - 1;
        $json_without_bigints = preg_replace('/:\s*(-?\d{' . $max_int_length . ',})/', ': "$1"', $data);
        $oresult              = json_decode($json_without_bigints, $outarray);
        return $oresult;
    }

 function paramOfdisciplineStatus(){
	 
		if($this->getData()->pagemenu=='amstododiscipline'){
			$fparam=array('dtype'=>'system','instanceid'=>1,'name'=>'gflex.webservice.discipline.todo');
			$pconfg=$this->getMconfig()->get_value($fparam);
			$pconfg=$this->castStringToArray($pconfg->value);
			$this->getData()->tododisciplinekey=$pconfg;
		}else if($this->getData()->pagemenu=='amscurriculum'){
			$fparam=array('dtype'=>'system','instanceid'=>1,'name'=>'gflex.webservice.discipline.completed');
			$pconfg=$this->getMconfig()->get_value($fparam);
			$pconfg=$this->castStringToArray($pconfg->value);
			$this->getData()->completeddisciplinekey=$pconfg;
		}
			
 } 
 
  function filterDisciplineShow($listdicipline){
	  if(empty($listdicipline) || !is_array($listdicipline)){return null;}
	
	  $this->paramOfdisciplineStatus();
	  $newlist=array();
	  if($this->getData()->pagemenu=='amstododiscipline'){
		  $listkeytodo= $this->getData()->tododisciplinekey;
			$listrdisc=$this->getUtildata()->getVaueOfArray($listdicipline,'data.disciplinas',true);
		 foreach($listrdisc as $row) {
			
			 $situacao=$this->getUtildata()->getVaueOfArray($row,'ocorrencia_disciplina');
			 if( in_array($situacao,  $listkeytodo)){array_push($newlist,$row);}
		 }
		 $listdicipline['data']['disciplinas']=$newlist;
	  }else if($this->getData()->pagemenu=='amscu=rriculum'){
		  
		
	  }
	  return  $listdicipline;
  }
 public function castStringToArray($text, $separator=',') {
		if($text==""){return null;}
		if(empty($separator)){return null;}
		$result=array();
		 $pos=stripos($text, $separator);
         if($pos=== false){
            $result=array($text);
        }else{
             $result= explode($separator, $text);
        }
		
		return $result;
	}
/**
     * Make request.
     *
     * @param $param
     * @return bool|mixed|string
     */
    private function request($param) {

		$queryst=http_build_query($param);
		$fparam=array('dtype'=>'system','instanceid'=>1,'name'=>'gflex.webservice.url');
		$host=$this->getMconfig()->get_value($fparam);
		$host=$host->value;
		$host.=$param;
		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$host);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	   
        $result = curl_exec($ch);
	    curl_close($ch);
        $result = $this->convertData($result);
		/*echo "<hr><pre>";
        print_r($result);
        echo "</pre>";*/
        return $result;
    }
	
	function count_completed() {
		
		$count=0;
		$result=new stdClass();
		$result->total=0;
		$result->completed=0;
		  
		$list=$this->getUtildata()->getVaueOfArray($this->getData()->historico,'data.disciplinas',true);
		
		foreach($list as $row) {
			$situacao=$this->getUtildata()->getVaueOfArray($row,'ocorrencia_disciplina');
			$result->total++;
			//if($situacao=='AM'){$result->completed++;}
			if(in_array($situacao,$this->getData()->completeddisciplinekey)){$result->completed++;}  
		}
		return $result;
	}
	
	function progress_bar($progress) {
			$result=new stdClass();
			$value="<div class=\"progressinfo pull-right\"><em>";
			if($progress->total==0){return "";}
            if($progress->completed > 0 && $progress->completed== $progress->total){
                $value.=get_string('completed', 'theme_badiumview');
                $perc=100;
            }else{
                      $value .= get_string('inprogress', 'theme_badiumview');
					  
                      if($progress->total > 0 && $progress->completed >= 0 && $progress->total >= $progress->completed ){
                          $perc=$progress->completed*100/$progress->total;
                        }
                      }
				 $percf=number_format($perc,2,',','');
				 $value.=" $percf% </em><br></div> "; 
				 
				$value.=" <div class=\"progress\">";
				$value.="<div class=\"progress-bar progress-bar-success progress-bar-striped active\" role=\"progressbar\" aria-valuenow=\"$perc\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:$perc%\"> &nbsp; </div>";
				$value.="</div>";
		return $value;
		
	}
	
	function get_title() {
		$title=$this->getUtildata()->getVaueOfArray($this->getData()->historico,'data.descricao_curso',true);
		return $title;
	}
	
	function  getCourseId($codigododiario) {
	if(empty($codigododiario)){return null;}
	 global $DB, $CFG;
	 $result=$DB->get_field('course', 'id', array('shortname'=>$codigododiario));
	 if(!empty($result)){return $result;}
	 
	 $sql="SELECT COUNT(id) AS  countrecord FROM {$CFG->prefix}course WHERE idnumber LIKE '".$codigododiario."%' ";
	 $r = $DB->get_record_sql($sql);
	
	 if(empty($r)){return null;}
	 if($r->countrecord ==0){return null; }
	 if($r->countrecord > 1){return -1; }
	 
	 $sql="SELECT id  FROM {$CFG->prefix}course WHERE idnumber LIKE '".$codigododiario."%' ";
	 $r = $DB->get_record_sql($sql);
	 if(!empty($r) && !empty($r->id )){return$r->id; }
     return null;
}

function  redirectToCourse() {
	$cdiario=optional_param('cdiario', NULL,PARAM_INT);
	if(empty($cdiario)){return null;}
	$courseid=$this->getCourseId($cdiario);

	if(empty($courseid)){
		$msg="";
		$msg.='<div class="alert alert-danger" role="alert">';
		$msg.="Não foi localizado nenhum curso no Moodle pelo código do diário $cdiario ";
		$msg.='</div>';
		echo $msg;
	}else if($courseid==-1){
		$msg="";
		$msg.='<div class="alert alert-danger" role="alert">';
		$msg.="Foi localizado mais de um curso no Moodle configurado com o código do diário $cdiario ";
		$msg.='</div>';
		echo $msg;
	}else{
		global $CFG;
		$url="$CFG->wwwroot/course/view.php?id=$courseid";
		redirect($url);
	}

     return null;
}


function  mdlCurrentDiscipline() {
	global $CFG;
	require_once("$CFG->dirroot/theme/badiumview/factory/my/template2/data.php");
	$badiumfview=new theme_badiumview_factory_my_template2_data();
	$this->getData()->pagetitle=$badiumfview->getData()->pagetitle;
	$badiumfview->getData()->messageshowinhead=null;
	$viewcontent="";
	ob_start();
	include_once("$CFG->dirroot/theme/badiumview/factory/my/template2/index_html.php");
   $viewcontent = ob_get_contents();
   ob_end_clean();
	echo  $viewcontent;
	
}
}



?>
