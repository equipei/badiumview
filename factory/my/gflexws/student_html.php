<div class="col-md-12"> 
	<?php echo $badiumfview->getData()->progresslabel; ?>
</div>
<div class="col-md-12">
	<?php 
	
	$listdata=$badiumfview->getUtildata()->getVaueOfArray($badiumfview->getData()->historico,'data.disciplinas',true);
	echo "<div>Total de disciplinas: ".sizeof($listdata)."</div>";
	?>
	<table class="table">
    <thead>
      <tr>
        <th>Código</th>
		<th>Disciplina</th>
		<th>Carga  horária</th>
		<th>Média</th>
		<th>Situação</th>
		<th></th>
      </tr>
    </thead>
    <tbody>
	<?php 
		$matricula=optional_param('matricula', NULL,PARAM_INT);
		if(empty($matricula)){$matricula="?";}
		else {$matricula="?matricula=$matricula&";}
		foreach($listdata as $row) {
			$tipo=$badiumfview->getUtildata()->getVaueOfArray($row,'tipo');
			$bloco=$badiumfview->getUtildata()->getVaueOfArray($row,'bloco');
			$codigo_diario=$badiumfview->getUtildata()->getVaueOfArray($row,'codigo_diario');
			$codigo_disciplina=$badiumfview->getUtildata()->getVaueOfArray($row,'codigo_disciplina');
			$descricao_disciplina=$badiumfview->getUtildata()->getVaueOfArray($row,'descricao_disciplina');
			$carga_horaria=$badiumfview->getUtildata()->getVaueOfArray($row,'carga_horaria');
			if(!empty($carga_horaria)){$carga_horaria.=" Horas";}
			$situacao=$badiumfview->getUtildata()->getVaueOfArray($row,'ocorrencia_disciplina');
			$media=$badiumfview->getUtildata()->getVaueOfArray($row,'media');
			$periodoletivo=$badiumfview->getUtildata()->getVaueOfArray($row,'periodoletivo');
			$linkcourso="";
			$url='view.php'.$matricula.'cdiario='.$codigo_diario;
			if(!empty($codigo_diario)){$linkcourso="<a href=\"$url\">Acessar </a>";}
			
		?>
	<tr>
        
		<td><?php echo $codigo_disciplina;?></td>
		<td><?php echo $descricao_disciplina;?></td>
		<td><?php echo $carga_horaria;?></td>
		<td><?php echo $media;?></td>
		<td><?php echo $situacao;?></td>
		<td><?php echo $linkcourso;?></td>
      </tr>
	<tr>
		<?php } ?>
	</tbody>
	
	</table>
	</div> 
