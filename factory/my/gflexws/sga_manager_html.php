<div class="col-md-12"> 
	<?php echo $badiumfview->getData()->progresslabel; ?>
</div>
<div class="col-md-12">
	<?php 
	
	$listdata=$badiumfview->getUtildata()->getVaueOfArray($badiumfview->getData()->historico,'data.disciplinas',true);
	echo "<div>Total de disciplinas: ".sizeof($listdata)."</div><br />";
	?>
	
	<div id="sgatrilha">
	<table  class="generaltable">
    <thead>
      <tr>
        <th>Codigo</th>
		<th>Disciplina</th>
		<th>Tipo</th>
        <th>Bloco</th>
        <th>Código do diário</th>
		<th>Carga  horária</th>
		<th>Média</th>
		<th>Período letivo</th>
		<th>Situação</th>
      </tr>
    </thead>
    <tbody>
	<?php 
		foreach($listdata as $row) {
			$tipo=$badiumfview->getUtildata()->getVaueOfArray($row,'tipo');
			$bloco=$badiumfview->getUtildata()->getVaueOfArray($row,'bloco');
			$codigo_diario=$badiumfview->getUtildata()->getVaueOfArray($row,'codigo_diario');
			$codigo_disciplina=$badiumfview->getUtildata()->getVaueOfArray($row,'codigo_disciplina');
			$descricao_disciplina=$badiumfview->getUtildata()->getVaueOfArray($row,'descricao_disciplina');
			$carga_horaria=$badiumfview->getUtildata()->getVaueOfArray($row,'carga_horaria');
			if(!empty($carga_horaria)){$carga_horaria.=" Horas";}
			$situacao=$badiumfview->getUtildata()->getVaueOfArray($row,'ocorrencia_disciplina');
			$media=$badiumfview->getUtildata()->getVaueOfArray($row,'media');
			$periodoletivo=$badiumfview->getUtildata()->getVaueOfArray($row,'periodoletivo');
		?>
	<tr>
        
		<td><?php echo $codigo_disciplina;?></td>
		<td><?php echo $descricao_disciplina;?></td>
		<td><?php echo $tipo;?></td>
        <td><?php echo $bloco;?></td>
         <td><?php echo $codigo_diario;?></td>
		<td><?php echo $carga_horaria;?></td>
		<td><?php echo $media;?></td>
		<td><?php echo $periodoletivo;?></td>
		<td><?php echo $situacao;?></td>
      </tr>
	<tr>
		<?php } ?>
	</tbody>
	
	</table>
	</div> 
	</div> 
