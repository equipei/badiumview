<?php 

//show message
if(!empty($badiumfview->getData()->messageshowinhead) && $badiumfview->getData()->countallcourseenrols > 0){
	echo "<div class=\"alert alert-warning\" role=\"alert\">".$badiumfview->getData()->messageshowinhead."</div>";
}

 //global without enrol
if($badiumfview->getData()->countallcourseenrols==0){echo $badiumfview->showMessageWithoutEnrol();}

//without split course by date
else if($badiumfview->getData()->countallcourseenrols > 0 && $badiumfview->getData()->pagemenu == 'factorymymenucurrentcourses'){
	if($badiumfview->getData()->showpast){
		include_once("tab.php"); 
		$currenttab= $badiumfview->getData()->pagemenu;
		print_tabs($tabs, $currenttab, $inactive, $activated); 
	}
	if($badiumfview->getData()->countcourseenrols==0){echo $badiumfview->showMessageWithoutEnrol();}
	else {
		echo $badiumfview->getCoursesCompletedProgressBar(array('data'=>$badiumfview->getData()));
		echo $badiumfview->coursesViewBootstrap(array('rows'=>$badiumfview->getData()->courseenrols,'bootstrapgridclass'=>'col-xs-12','containerid'=>'coursecardtrail1','maxcardperrow'=>1,'isteachear'=>$badiumfview->getData()->isteachear)); //one cart 0px - 768px
		echo $badiumfview->coursesViewBootstrap(array('rows'=>$badiumfview->getData()->courseenrols,'bootstrapgridclass'=>'col-md-6','containerid'=>'coursecardtrail2','maxcardperrow'=>2,'isteachear'=>$badiumfview->getData()->isteachear)); //thwo cart 768px - 1100px
		echo $badiumfview->coursesViewBootstrap(array('rows'=>$badiumfview->getData()->courseenrols,'bootstrapgridclass'=>'col-md-4','containerid'=>'coursecardtrail3','maxcardperrow'=>3,'isteachear'=>$badiumfview->getData()->isteachear)); //tree cart 1101px - 1400px
		echo $badiumfview->coursesViewBootstrap(array('rows'=>$badiumfview->getData()->courseenrols,'bootstrapgridclass'=>'col-lg-3','containerid'=>'coursecardtrail4','maxcardperrow'=>4,'isteachear'=>$badiumfview->getData()->isteachear));  //four cart >= 1401px 
	}
}

// width split course by date
 else if($badiumfview->getData()->countallcourseenrols > 0 && $badiumfview->getData()->pagemenu == 'factorymymenupreviouscourses'){ 
 
	include_once("tab.php");
	$currenttab= $badiumfview->getData()->pagemenu;
	print_tabs($tabs, $currenttab, $inactive, $activated); 

	if($badiumfview->getData()->past->countcourseenrols==0){echo $badiumfview->showMessageWithoutEnrol();}
	else {
		
		echo $badiumfview->getCoursesCompletedProgressBar(array('data'=>$badiumfview->getData()->past));
	
		echo $badiumfview->coursesViewBootstrap(array('rows'=>$badiumfview->getData()->past->courseenrols,'bootstrapgridclass'=>'col-xs-12','containerid'=>'coursecardtrail1','maxcardperrow'=>1,'isteachear'=>$badiumfview->getData()->isteachear)); //one cart 0px - 768px
		echo $badiumfview->coursesViewBootstrap(array('rows'=>$badiumfview->getData()->past->courseenrols,'bootstrapgridclass'=>'col-md-6','containerid'=>'coursecardtrail2','maxcardperrow'=>2,'isteachear'=>$badiumfview->getData()->isteachear)); //thwo cart 768px - 1100px
		echo $badiumfview->coursesViewBootstrap(array('rows'=>$badiumfview->getData()->past->courseenrols,'bootstrapgridclass'=>'col-md-4','containerid'=>'coursecardtrail3','maxcardperrow'=>3,'isteachear'=>$badiumfview->getData()->isteachear)); //tree cart 1101px - 1400px
		echo $badiumfview->coursesViewBootstrap(array('rows'=>$badiumfview->getData()->past->courseenrols,'bootstrapgridclass'=>'col-lg-3','containerid'=>'coursecardtrail4','maxcardperrow'=>4,'isteachear'=>$badiumfview->getData()->isteachear));  //four cart >= 1401px 
	}
}?>