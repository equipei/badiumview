<?php
require_once("$CFG->dirroot/theme/badiumview/locallib/badiunet/locallib.php");
require_once("$CFG->dirroot/theme/badiumview/factory/basedata.php");
class theme_badiumview_factory_my_badiuamstrail_data extends  theme_badiumview_factory_basedata {
 
 function __construct() {
	 parent::__construct();
		$this->init();
    }
	 
 function init() { 
	$fc=new theme_badiumview_badiunetclient_locallib();
	$fc->addParamdefault('_key','badiu.admin.client.amstrail.dashboard');
	$fc->exec(); 
	$data=$fc->getContent();
	$traildata=$this->getUtildata()->getVaueOfArray($data,'message.content.badiu_list_data_rows',true);
	$this->getData()->amstrail=$traildata;
	$this->getData()->pagetitle='Minha Trilha';
	
}	

 function showCurrilumDisciplines($offerid) { 
 
	$curriculumdiscipline=$this->getUtildata()->getVaueOfArray($this->getData()->amstrail,'data.2',true);
  $html="
	<table class=\"table\">
    <thead>
      <tr>
        <th>Id</th>
		<th>Disciplina</th>
        <th>Matricula</th>
        <th>Acessar</th>
      </tr>
    </thead>
    <tbody>";
	foreach ($curriculumdiscipline as $discipline){
		$did=$this->getUtildata()->getVaueOfArray($discipline,'disciplineid');
		$dname=$this->getUtildata()->getVaueOfArray($discipline,'disciplinename');
		$dstatus=$this->getUtildata()->getVaueOfArray($discipline,'statusshortname');
		$dofferid=$this->getUtildata()->getVaueOfArray($discipline,'offerid');
		if($dofferid==$offerid){
			$linkaccess="";
			$infoenrol="";
			$urlaccess="";
			if(!empty($dstatus)){$infoenrol="<i  style=\"color:green\" class=\"fa fa-circle\" aria-hidden=\"true\"></i>Matriculado";}
			else {$infoenrol="<i style=\"color:gray\" class=\"fa fa-circle\" aria-hidden=\"true\"></i>Não matriculado";}
			
			if(!empty($dstatus)){$urlaccess="Não liberado";}
			else {$urlaccess="<a href='#'>Acessar</a>";}
			
		$html.="	
			<tr>
				<td>$did</td>
				<td>$dname</td>
				<td>$infoenrol</td>
				<td>$urlaccess</td>
			</tr>";
			}
		} 
		$html.="  </tbody></table>";
		
	return $html;
}

}
?>
