<?php
require_once("../../../../../config.php");
require_login();
require_once("data.php");
$badiumfview=new theme_badiumview_factory_my_template1_data();
$viewcontent="";
ob_start();
   include_once("index_html.php");
   $viewcontent = ob_get_contents();
ob_end_clean();
 
$PAGE->set_context($context);
$PAGE->set_url('/theme/badiumview/factory/my/template1/view.php');
$PAGE->set_pagelayout('standard'); 
$PAGE->set_title($badiumfview->getData()->pagetitle);
$PAGE->set_heading($badiumfview->getData()->pagetitle); 

echo $OUTPUT->header();
 echo $viewcontent;
echo $OUTPUT->footer();

 
?>

