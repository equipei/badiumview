<?php 

//show message
if(!empty($badiumfview->getData()->messageshowinhead) && $badiumfview->getData()->countallcourseenrols > 0){
	echo "<div class=\"alert alert-warning\" role=\"alert\">".$badiumfview->getData()->messageshowinhead." </div>";
}

 //global without enrol
if($badiumfview->getData()->countallcourseenrols==0){echo $badiumfview->showMessageWithoutEnrol();}

//without split course by date
else if($badiumfview->getData()->countallcourseenrols > 0 && $badiumfview->getData()->pagemenu == 'factorymymenucurrentcourses'){
	if($badiumfview->getData()->showpast){
		include_once("tab.php"); 
		$currenttab= $badiumfview->getData()->pagemenu;
		print_tabs($tabs, $currenttab, $inactive, $activated); 
	}
	if($badiumfview->getData()->countcourseenrols==0){echo $badiumfview->showMessageWithoutEnrol();}
	else { 
		echo $badiumfview->getCoursesCompletedProgressBar(array('data'=>$badiumfview->getData()));
		echo $badiumfview->coursesViewTable(array('rows'=>$badiumfview->getData()->courseenrols,'isteachear'=>$badiumfview->getData()->isteachear));
	}
}
 
// width split course by date
 else if($badiumfview->getData()->countallcourseenrols > 0 && $badiumfview->getData()->pagemenu == 'factorymymenupreviouscourses'){ 
	include_once("tab.php");
	$currenttab= $badiumfview->getData()->pagemenu;
	print_tabs($tabs, $currenttab, $inactive, $activated); 

	if($badiumfview->getData()->past->countcourseenrols==0){echo $badiumfview->showMessageWithoutEnrol();}
	else {
		echo $badiumfview->getCoursesCompletedProgressBar(array('data'=>$badiumfview->getData()->past));
		echo $badiumfview->coursesViewTable(array('rows'=>$badiumfview->getData()->past->courseenrols,'isteachear'=>$badiumfview->getData()->isteachear));
	}
}?>