<?php
require_once("$CFG->dirroot/theme/badiumview/locallib/pluginconfig.php");
require_once("$CFG->dirroot/theme/badiumview/factory/basedata.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/course/trailcourselib.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/course/rolelib.php");

class theme_badiumview_factory_my_locallib extends  theme_badiumview_factory_basedata {
  function __construct() {
		parent::__construct();
	}

public function init() { 
		global $USER;
		
		$trailcourselib=new theme_badiumview_course_trailcourselib();
		$themeconfig=new theme_badiumview_pluginconfig();
		$rolelib=new theme_badiumview_course_rolelib();
		$userid=$USER->id;
		$this->getData()->pagemenu=optional_param('_badiupagemenu','factorymymenucurrentcourses',PARAM_TEXT);
		
		$param=array('coursevisible'=>1,'userid'=>$userid,'enrolstatus'=>0,'enrolmethodstatus'=>0,'enroltimevalidate'=>1,'showcoursecategorypath'=>1);  
		//coursetimestart 
	
		$coursetimestart=$this->getCourseTimestart();
		
		$this->getData()->showpast=false;
		if($coursetimestart!=null && $coursetimestart!='' &&  $coursetimestart > 0){
			$param['coursetimestart']=$coursetimestart; 
			$this->getData()->showpast=true;
		}
		 
		$this->getData()->isteachear=false;
		$this->getData()->istudent=true;
		
		$isteachear=$rolelib->hasTeachearProfile($userid);
		if($isteachear){
			unset($param['coursevisible']);
			$this->getData()->isteachear=true;
			$this->getData()->istudent=false;
		}
		
		$this->getData()->countcourseenrols=$trailcourselib->getEnrolsCount($param);
		$this->getData()->countallcourseenrols=$this->getData()->countcourseenrols;		
		if($this->getData()->pagemenu=='factorymymenucurrentcourses'){
			$this->getData()->courseenrols=$trailcourselib->getEnrols($param);
			$this->getData()->countcourseenrolscompletationenabled=$trailcourselib->getEnrolsCountCompletationEnabled($param);
			$this->getData()->countcourseenrolscompletationend=$trailcourselib->getEnrolsCountCompletationEnd($param);
		}
		
		//couses past
		if($coursetimestart!=null && $coursetimestart!='' &&  $coursetimestart > 0){
			$param['coursetimestartoperator']= " <= ";
			$datapast=new stdClass();
			
			$datapast->countcourseenrols=$trailcourselib->getEnrolsCount($param);
			$this->getData()->countallcourseenrols+=$datapast->countcourseenrols;
			
			$datapast->courseenrols=array();
			$datapast->countcourseenrolscompletationenabled=0;;
			$datapast->countcourseenrolscompletationend=0;
			
			if($this->getData()->pagemenu=='factorymymenupreviouscourses'){
				$datapast->courseenrols=$trailcourselib->getEnrols($param);
				$datapast->countcourseenrolscompletationenabled=$trailcourselib->getEnrolsCountCompletationEnabled($param);
				$datapast->countcourseenrolscompletationend=$trailcourselib->getEnrolsCountCompletationEnd($param);
			}
			$this->getData()->past=$datapast; 
			
		}
		
		$this->setPageTile();
		
		//custom message
		$fparam=array();
		$fparam=array('dtype'=>'system','instanceid'=>1,'name'=>'course.head');
		//$factorymymessageshowinhead=$this->getMconfig()->get_value($fparam);
		//if(!empty($factorymymessageshowinhead->value)){$factorymymessageshowinhead=$factorymymessageshowinhead->value;}		
		if(empty($factorymymessageshowinhead)){$factorymymessageshowinhead=$themeconfig->getValue('factorymymessageshowinhead');}
		$this->getData()->messageshowinhead=$factorymymessageshowinhead;
	}

	public function setPageTile() {
		if($this->getData()->isteachear){$this->getData()->pagetitle=get_string('factorymytitleforteachear', 'theme_badiumview');}
		else {$this->getData()->pagetitle=get_string('factorymytitleforstudent', 'theme_badiumview');}
			
	}
	 public function getCourseTimestart() {
		 $themeconfig=new theme_badiumview_pluginconfig();
		 $coursebytimestart=$themeconfig->getValue('factorymyfitercoursebytimestart');
		 if(!empty($coursebytimestart)){
			try{
				$coursebytimestart = date_create_from_format('Y/m/d', $coursebytimestart);
				if (is_a($coursebytimestart, 'DateTime')) {
				$coursebytimestart=$coursebytimestart->getTimestamp();
				}
			}
			catch (Throwable $t){$coursebytimestart=null;}
			
		}
		 
        return $coursebytimestart;
    }
   
    public function getStudentCourseStatus($param) {
			$data=$this->getUtildata()->getVaueOfArray($param,'row');
			$value=null;
            $activity=null;
            $activitycompleted=null;
            $completed=false;
            $perc=0;
            
            if(isset($data->timecompleted) && $data->timecompleted > 0){$completed=true;}
            if(isset($data->countactivityanebleprogress)){$activity=$data->countactivityanebleprogress;}
            if(isset($data->countactivitycompleted)){$activitycompleted=$data->countactivitycompleted;}
            if($completed){
                $value=get_string('completed', 'theme_badiumview');
                $perc=100;
            }else{
                      $value = get_string('inprogress', 'theme_badiumview');
					  
                      if($activity > 0 && $activitycompleted >= 0 && $activity >= $activitycompleted ){
                          $perc=$activitycompleted*100/$activity;
                           if($perc==0 && empty($data->timelastaccess)){$value = get_string('notstarted', 'theme_badiumview');}
                        }
                      }
				 $percf=number_format($perc,2,',','');
				 $value.=" $percf% <br> ";
				 
				$value.=" <div class=\"progress\">";
				$value.="<div class=\"progress-bar progress-bar-success progress-bar-striped active\" role=\"progressbar\" aria-valuenow=\"$perc\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:$perc%\"> &nbsp; </div>";
				$value.="</div>";
		return $value;
	  } 
	
	function coursesViewTable($param) { 
		$rows=$this->getUtildata()->getVaueOfArray($param,'rows');
		$isteachear=$this->getUtildata()->getVaueOfArray($param,'isteachear');
		
		$table= new html_table();
	    $columns=array();
		
		global $CFG;
		
		$rowsviews=array();
   
   

		foreach ($rows as $row){         
			$row=$this->getCourseLink($row);
			$status= $this->getStudentCourseStatus(array('row'=>$row));
	
			$timelastaccess=$row->timelastaccess;
			if($timelastaccess){$timelastaccess=date('d/m/Y  H:i:s',$timelastaccess);}
			$newrow=array('id'=>$row->id,'fullname'=>$row->fullname,'status'=>$status,'timelastaccess'=>$timelastaccess);
			if($isteachear){unset($newrow['status']);}
         
			array_push($rowsviews,$newrow);
		}
  
		$table->head =$columns;
		$table->head=array('id' =>get_string('id', 'theme_badiumview'), 'fullname' => get_string('course', 'theme_badiumview'), 'status' => get_string('status', 'theme_badiumview') ,'timelastaccess'=>get_string('lastaccess', 'theme_badiumview'));
		if($isteachear){$table->head=array('id' => get_string('id', 'theme_badiumview'), 'fullname' => get_string('course', 'theme_badiumview'),'timelastaccess'=>get_string('lastaccess', 'theme_badiumview'));}
		$table->data=$rowsviews;
		return  '<div id="sgatrilha">'. html_writer::table($table) ."</div>";
	}
	
	function coursesViewBootstrap($param){     
		$rows=$this->getUtildata()->getVaueOfArray($param,'rows');
		$bootstrapgridclass=$this->getUtildata()->getVaueOfArray($param,'bootstrapgridclass');
		$containerid=$this->getUtildata()->getVaueOfArray($param,'containerid');
		$maxcardperrow=$this->getUtildata()->getVaueOfArray($param,'maxcardperrow');
		$isteachear=$this->getUtildata()->getVaueOfArray($param,'isteachear');
	
		$gridcont=1;
		$cont=0;
		$html="";
		$html.="<div id=\"$containerid\">";  
		$starrow="<div class=\"row\" >";
		$endrow="</div><br />";

    
		global $CFG;
      foreach ($rows as $row) {
           
			$status= $this->getStudentCourseStatus(array('row'=>$row));
           $coursename=$row->fullname;
		    $coursename=$this->limitTextCharactar($coursename,50);
			$courseid=$row->id;
			$courseimgurl=$this->getCourseUrlImage($courseid);
			if(empty($courseimgurl)){$courseimgurl="defaultimagecourse.jpg";}
			$urlcourse= $CFG->httpswwwroot."/course/view.php?id=$courseid";
			$linkcourse="<a href=\"$urlcourse\"></a>";
			$coursecatinfo="";
			if(isset($row->categoryname)){$coursecatinfo="<div class=\"coursecatnamenote\">$row->categoryname</div>";}
	 
			  if($gridcont==1){$html.= $starrow;}
              $html.="<div class=\"$bootstrapgridclass\">";
               $html.="<div class=\"card\" >";
				 $html.="<a href=\"$urlcourse\"><img class=\"card-img-top\" src=\"$courseimgurl\" alt=\"Card image cap\"></a>";
				$html.="<div class=\"card-block \"><div class=\"course-title\"><a href=\"$urlcourse\">$coursename </a></div>$coursecatinfo</div>";
                if(!$isteachear) {$html.="<div class=\"card-block\"><a href=\"$urlcourse\">$status</a></div>";}
				else{$html.="<div class=\"card-block\"><div class=\"card-course-access\"><a href=\"$urlcourse\" class=\"btn btn-default\" role=\"button\">Acessar</a></div></div>";}
              $html.=" </div>"; //end card
              $html.=" </div>"; //end  col
  
              $gridcont++; 
              $cont++; 
              if($gridcont > $maxcardperrow){
                  $gridcont=1;
                  $html.= $endrow;
              } else if($cont==count($rows)){$html.=   $endrow;}
              
        
          } //end foreach ($litem as $item) {
       $html.="</div>";
       return  $html;
    }
	
	
	 function getCourseLink($row) {
      global $CFG;
      $courseid=$row->id; 
      $coursename=$row->fullname; 
      $courseurl=$CFG->httpswwwroot."/course/view.php?id=$courseid";
      $courselink="<a href=\"$courseurl\">$coursename</a>";
     
	  $coursecatinfo="";
	  if(isset($row->categoryname)){$coursecatinfo="<br /><div class=\"coursecatnamenote\">$row->categoryname</div>";}
	  $courselink.=$coursecatinfo;
	  $row->fullname=$courselink;
      return $row;
 }
 
 function getCourseUrlImage($courseid)
    {
        $url = '';
		global $CFG;
		require_once( $CFG->libdir . '/filelib.php' );
       $context = context_course::instance($courseid);
       $fs = get_file_storage();
       $files = $fs->get_area_files( $context->id, 'course', 'overviewfiles', 0 );
       foreach ( $files as $f ){
         if ( $f->is_valid_image() )
         {
            $url = moodle_url::make_pluginfile_url( $f->get_contextid(), $f->get_component(), $f->get_filearea(), null, $f->get_filepath(), $f->get_filename(), false );
         }
       }
		return $url;
    }
	
	 function getCoursesCompletedProgressBar($param) {
		$data=$this->getUtildata()->getVaueOfArray($param,'data');
		$countcourseenrol=$data->countcourseenrolscompletationenabled;
		$countcoursecompleted=$data->countcourseenrolscompletationend;

		$perc=0;
		if($countcourseenrol >=0 && $countcoursecompleted >=0 && $countcourseenrol>= $countcoursecompleted && $countcourseenrol >0){
			$perc= $countcoursecompleted*100/$countcourseenrol;
		}
		else {return "";}
		$percf=number_format($perc,2,',','');
		$labelcousescompletd=get_string('coursescompleted', 'theme_badiumview');
		$perctext=$percf."%";
		$label="<span id=\"disciplineprocess\">$labelcousescompletd $countcoursecompleted de $countcourseenrol ($perctext)</span> <br />";
		///  $label.="<progress style=\" width: 100%\"  value=\"$perc\" max=\"100\" ></progress>";
		$label.=" <div class=\"progress\">";
		$label.="<div class=\"progress-bar progress-bar-success progress-bar-striped active\" role=\"progressbar\" aria-valuenow=\"$perc\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:$perc%\"> $percf % </div>";
		$label.="</div><br />";
		return $label;
}


 function limitTextCharactar($txt,$limitchar=50)  {
     $out = strlen($txt) > $limitchar ? substr($txt,0,$limitchar-3)."..." : $txt;
	 return $out;
	}
	
 function showMessageWithoutEnrol($param=array())  { 
		$msg=get_string('withoutenrolmessage', 'theme_badiumview');
		$out="<div class=\"alert alert-warning\" role=\"alert\"> $msg</div>";

		return $out;
	}	
}
