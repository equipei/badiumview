<?php
require_once($CFG->dirroot.'/theme/badiumview/factory/locallib.php');
require_once("$CFG->dirroot/theme/badiumview/locallib/factorycoursedata.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/enrollib.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/pluginconfig.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/baselib.php");
require_once("$CFG->dirroot/theme/badiumview/factory/basedata.php");
require_once("$CFG->dirroot/theme/badiumview/core/jsonutil.php");

class theme_badiumview_factory_proxyview extends  theme_badiumview_factory_basedata {
     
	function __construct(){
		parent::__construct();
	}
	
	public function getConfig(){
		global $CFG;
		$config=new  stdClass();
		$config->wwwroot=$CFG->wwwroot;
		$config->dataroot=$CFG->dataroot;
		return $config;
	}
	
	
	//echo $CFG->release;exit;
	public function column2($templatecontext) { 
		global $COURSE;
		global $USER;
		global $CFG;
		$llib=new theme_badiumview_factory_locallib();
		$fccontent=new theme_badiumview_factorycoursedata();
		$themeconfig=new theme_badiumview_pluginconfig();
		$enrollib=new theme_badiumview_enrollib();
		
		$currenturi = $llib->clean_uri();
		
		
		$ref = $_SERVER['REQUEST_URI'];
		$refmy = strpos($currenturi, '/my/');
		$refcourse = strpos($currenturi, '/course/index.php');
		$refcourseview = strpos($currenturi, '/course/view.php');
		
		$fviwdata=new stdClass();
		$fviwdata->courseid=0;
		$fviwdata->coursename=null;
		$fviwdata->courseprogress=array();
		$fviwdata->iscoursecontext=0;
		$fviwdata->topics=array();
		$fviwdata->isstudent=0;
		$fviwdata->isstudentincoursecontext=0;

		$fviwdata->isstudentinmycontext=0;
		$fviwdata->contentstudentmy="";
		$fviwdata->maincontent="";
		
		$fviwdata->enableblockscontrolshow=$themeconfig->getValue('enableblockscontrolshow');

		if (isset($COURSE->id) && $COURSE->id > 0 ){
			$fviwdata->courseid=$COURSE->id;
			$fviwdata->coursename=$COURSE->fullname;
		}
		
		
		$mytemplate=$themeconfig->getValue('factorymy');
		$factorycourseformat=$themeconfig->getValue('factorycourseformat');

		if ($fviwdata->courseid > 0 && $refcourseview === 0 && !empty($factorycourseformat)){
			$fviwdata->iscoursecontext=1;
			$fviwdata->isstudent=$enrollib->is_student_course($COURSE->id, $USER->id);
			if($fviwdata->isstudent){
				$fitterparamtopic=array('courseid'=>$COURSE->id,'visible'=>1,'userid'=>$USER->id,'addactivities'=>1,'addactivitiesitemhtml'=>1,'countaccess'=>1);
				//$fviwdata->topics=$fccontent->getTopics($fitterparamtopic);
	
				$fviwdata->isstudentincoursecontext=1;
				//$fviwdata->courseprogress=$fccontent->getCourseProgress($fviwdata->topics);
				$tplresult=$llib->getContent('factorycourseformat',$factorycourseformat);
				$fviwdata->maincontent=$tplresult->content;
				$badiumfview=$tplresult->data;
				$fviwdata->courseprogress=$fccontent->getCourseProgress($badiumfview->getData()->topics);
			// echo $fviwdata->fchtmlontent;exit;
			}
		}

		

		//my
		if ( isloggedin() && ($refmy === 0)) {
			//$fviwdata->isstudentinmycontext=0;
			//$fviwdata->contentstudentmy="xxxx";
   
		}   
		$this->config_redirect(); 
		if ( isloggedin() && ($refmy === 0 || $currenturi=='/' || $currenturi=='/index.php'|| $currenturi=='/index.php?')) {
			
			$url=null;
			$mytemplate=$themeconfig->getValue('factorymy');
			
			$mytemplatekey=str_replace('factorymy','',$mytemplate);
			
		    if(!empty($mytemplatekey)){
				$redirect=$enrollib->is_student();
				$url="$CFG->wwwroot/theme/badiumview/factory/my/$mytemplatekey/view.php";
				if(!$redirect){$redirect=$enrollib->is_teachear();}
				if(!$redirect){$cenrol=$enrollib->count_enrol();if($cenrol >=0){$redirect=true;}}
				if(!empty($url) && $redirect){redirect($url);}
			}
			
		}   
		//review delete
		$layout     = optional_param('_layout', null, PARAM_TEXT);
		$showmenupath=true;
		if(!empty($layout)){$showmenupath=false;}
		
		$fviwdata->config=$this->getConfig();
		$templatecontext['fviwdata']=$fviwdata;
		
		$fviwdata->config=$this->getConfig();
		$fviwdata->template=new  stdClass();
		$fviwdata->template->header=null;
		$fviwdata->template->footer=null;
		$fviwdata->template->cssurl=null;

		//if version less than 5.2 
		/*$fviwdata->template->moodlelastverison=true;
		if($CFG->version < 2018051702){$fviwdata->template->moodlelastverison=false;}
		*/
		$tparam=new  stdClass();
		$tparam->layout='themecoretemplate'; //themecoretemplate | themelogintemplate
		$tparam->mustache=new  stdClass();
		$tparam->mustache->data=$templatecontext;

		$factorytheme=$themeconfig->getValue('factorytheme');
		//$factorytheme='florence';
		$templatecontext['fviwdata']=$fviwdata;
		if(!empty($factorytheme)){
			$tresult=$llib->getContent('factorytheme',$factorytheme,$tparam);
			$fviwdata->template->header=$tresult->header;
			$fviwdata->template->footer=$tresult->footer;
			$fviwdata->template->cssurl=$tresult->cssurl;
			$fviwdata->urlwebview=$tresult->urlwebview;
		}
		 
		$templatecontext['fviwdata']=$fviwdata;
		return $templatecontext;
	}
	
	
	public function login($templatecontext) { 
		global $COURSE;
		global $USER;
		global $CFG;
		$llib=new theme_badiumview_factory_locallib();
		$themeconfig=new theme_badiumview_pluginconfig();
			
		$fviwdata=new stdClass();
		$fviwdata->config=$this->getConfig();
		$templatecontext['fviwdata']=$fviwdata;
		
		
		$fviwdata->config=$this->getConfig();
		$fviwdata->template=new  stdClass();
		$fviwdata->template->header=null;
		$fviwdata->template->footer=null;
		$fviwdata->template->cssurl=null;

		//if version less than 5.2 
		/*$fviwdata->template->moodlelastverison=true;
		if($CFG->version < 2018051702){$fviwdata->template->moodlelastverison=false;}*/

		$tparam=new  stdClass();
		$tparam->layout='themelogintemplate'; //themecoretemplate | themelogintemplate
		$tparam->mustache=new  stdClass();
		$tparam->mustache->data=$templatecontext;

		$factorytheme=$themeconfig->getValue('factorytheme');
		//$factorytheme='florence';
		$templatecontext['fviwdata']=$fviwdata;
		if(!empty($factorytheme)){
			$tresult=$llib->getContent('factorytheme',$factorytheme,$tparam);
			$fviwdata->template->header=$tresult->header;
			$fviwdata->template->maincontent=null;
			if(isset($tresult->form->content)){$fviwdata->template->maincontent=$tresult->form->content;}
			$fviwdata->template->footer=$tresult->footer;
			$fviwdata->template->cssurl=$tresult->cssurl;
			$fviwdata->urlwebview=$tresult->urlwebview;
		}
		
		$templatecontext['fviwdata']=$fviwdata;
		return $templatecontext;
	}
	
	public function get_core_version() { 
		global $CFG;
		$baselib=new theme_badiumview_baselib();
		$mdlversion=$CFG->release;
		$p=explode(".",$mdlversion);
		$v1=$baselib->getUtildata()->getVaueOfArray($p, 0);
		$v2=$baselib->getUtildata()->getVaueOfArray($p, 1);
		$v=$v1.'.'.$v2;
		return $v;
	}
	public function get_layout_file($type='columns2') { 
		global $CFG;
		$defaulversion="3.10";
		$cversion=$this->get_core_version();
		$file=$type.'.php';
		$tfile="$CFG->dirroot/theme/badiumview/layout/$cversion/$file";
		if(!file_exists ($tfile)){
			$tfile="$CFG->dirroot/theme/badiumview/layout/$defaulversion/$file";
		}
		return $tfile;
		
	}

	public function config_redirect() { 
		if(!isloggedin()){return null;}
		$enrollib=new theme_badiumview_enrollib();
		if($enrollib->is_admin()){return null;}

		$keybase="badiu.moodle.proxy";
		$fparam=array('dtype'=>'system','instanceid'=>1,'name'=>$keybase,'_operatorname'=>'LIKEEND');
		$factorymymessageshowinhead=$this->getMconfig()->get_value($fparam);
		$urlfpconfig=$this->getMconfig()->get_values($fparam); 
		if(!is_array($urlfpconfig)){return null;}
		$urlfpconfiglist=array();
		
		
		foreach ($urlfpconfig as $rowc){
			$urlfpconfiglist[$rowc->name]=$rowc;
		}


		$shortnameroles=$enrollib->get_enrol_course_shortname_roles();
		if(!is_array($shortnameroles)){return null;}
		$shortnameroleslist=array();
		foreach ($shortnameroles as $rowr){
			$shortnameroleslist[$rowr->shortname]=$rowr->shortname;
		}

		$isadmin=$enrollib->is_admin();
		if($isadmin){
			$adminkey=$keybase.'.admin';
			$itemconfig=$this->getUtildata()->getVaueOfArray($urlfpconfiglist,$adminkey);
			$adminurl=$itemconfig->value;
			$isredirct=false;
			if(!empty($adminurl)){$isredirct=$this->check_config_url_redirect($itemconfig);}
			if(!empty($adminurl) && $isredirct && filter_var($adminurl, FILTER_VALIDATE_URL)) {redirect($adminurl);}

		}
		$studentkey=$keybase.'.student';
		$student=$this->getUtildata()->getVaueOfArray($shortnameroleslist,'student');
		$itemconfig=$this->getUtildata()->getVaueOfArray($urlfpconfiglist,$studentkey);
		$studenturl=$itemconfig->value;
		$isredirct=false;
		if(!empty($student)){$isredirct=$this->check_config_url_redirect($itemconfig);}
		if(!empty($student) && !empty($studenturl) && $isredirct && filter_var($studenturl, FILTER_VALIDATE_URL)) {redirect($studenturl);}

		$teacherkey=$keybase.'.teacher';
		$teacher=$this->getUtildata()->getVaueOfArray($shortnameroleslist,'teacher');
		$itemconfig=$this->getUtildata()->getVaueOfArray($urlfpconfiglist,$teacherkey);
		$teacherurl=$itemconfig->value;
		$isredirct=false;
		if(!empty($teacher)){$isredirct=$this->check_config_url_redirect($itemconfig);}
		
		if(!empty($teacher) && !empty($teacherurl) && $isredirct  && filter_var($teacherurl, FILTER_VALIDATE_URL)) {redirect($teacherurl);}

		$editingteacherkey=$keybase.'.editingteacher';
		$editingteacher=$this->getUtildata()->getVaueOfArray($shortnameroleslist,'editingteacher');
		$itemconfig=$this->getUtildata()->getVaueOfArray($urlfpconfiglist,$editingteacherkey);
		$editingteacherurl=$itemconfig->value;
		$isredirct=false;
		if(!empty($editingteacher)){$isredirct=$this->check_config_url_redirect($itemconfig);}
		
		if(!empty($editingteacher) && !empty($editingteacherurl) && $isredirct  && filter_var($editingteacherurl, FILTER_VALIDATE_URL)) {redirect($editingteacherurl);}
		
		$managerkey=$keybase.'.manager';
		$manager=$this->getUtildata()->getVaueOfArray($shortnameroleslist,'manager');
		$itemconfig=$this->getUtildata()->getVaueOfArray($urlfpconfiglist,$managerkey);
		$managerurl=$itemconfig->value;
		$isredirct=false;
		if(!empty($manager)){$isredirct=$this->check_config_url_redirect($itemconfig);}
		
		if(!empty($manager) && !empty($managerurl) && $isredirct  && filter_var($managerurl, FILTER_VALIDATE_URL)) {redirect($managerurl);}
		
		//no default url
		foreach ($shortnameroles as $rowr){
			$rolekey=$keybase.'.'.$rowr->shortname;
			$roleurl=$this->getUtildata()->getVaueOfArray($urlfpconfiglist,$rolekey);
			$itemconfig=$this->getUtildata()->getVaueOfArray($urlfpconfiglist,$managerkey);
			$isredirct=false;
			if(!empty($roleurl)){$isredirct=$this->check_config_url_redirect($itemconfig);}
		
		
			if(!empty($roleurl) && $isredirct  && filter_var($roleurl, FILTER_VALIDATE_URL)) {break;redirect($roleurl);}

		}
		//defult redirect
		$defaulturl=$this->getUtildata()->getVaueOfArray($urlfpconfiglist,$keybase);
		if(!empty($defaulturl) && filter_var($defaulturl, FILTER_VALIDATE_URL)) {redirect($defaulturl);}
		
	}
	
	public function check_config_url_redirect($config) { 
		$dconfig=$config->dconfig;
		
		if(empty($dconfig)){return false;}
		$jsonutil=new theme_badiumview_core_jsonutil();
		$dconfig=$jsonutil->decode($dconfig);
		
		if(!is_array($dconfig)){return false;}
		$llib=new theme_badiumview_factory_locallib();
		$currenturi = $llib->clean_uri();
		
		$listuritoredirect=$this->getUtildata()->getVaueOfArray($dconfig,'sproxy');
		foreach ($listuritoredirect as $uri){
			$ref = strpos($currenturi, $uri);
			if($ref === 0){return true;} 
		}
		$listproxy=$this->getUtildata()->getVaueOfArray($dconfig,'cproxy');
		foreach ($listproxy as $row){
			$from=$this->getUtildata()->getVaueOfArray($row,'from');
			$to=$this->getUtildata()->getVaueOfArray($row,'to');
			if(!empty($from) && !empty($to) && filter_var($to, FILTER_VALIDATE_URL)){
				$ref = strpos($currenturi, $from);
				if($ref === 0){redirect($to);return null;} 
			}
		}
		return false;
	}
} 
