<?php
require_once("$CFG->dirroot/theme/badiumview/locallib/formutil.php");
class theme_badiumview_factory_formutil extends theme_badiumview_formutil {
     
	function __construct(){
		
	}
	public function getOptionMyTemplate(){
 		$op=array();
		$op['']='';
        $op['mytemplate1']=get_string('factorymytemplate1','theme_badiumview');
        $op['mytemplate2']=get_string('factorymytemplate2','theme_badiumview');
        return $op;
 	}
	public function getOptionCourseFormatTemplate(){
 		$op=array();
		$op['']='';
       /* $op['courseformattemplate1']=get_string('factorycourseformattemplate1','theme_badiumview');
        $op['courseformattemplate2']=get_string('factorycourseformattemplate2','theme_badiumview');*/
		
		$op=$this->getOptionsInFolder('courseformat',false);
		$op1=$this->getOptionsInFolder('courseformat',true);
		foreach ($op1 as $k => $v) {$op[$k]=$v;}
		
        return $op;
 	}

	public function getOptionTemplate($template){
 		$op=array();
		$op['']='';
       
		$op=$this->getOptionsInFolder($template,false);
		$op1=$this->getOptionsInFolder($template,true);
		foreach ($op1 as $k => $v) {$op[$k]=$v;}
	    return $op;
 	}
	
    /**
     * get list of course format in themefolder
     */
    public function getOptionsInFolder($type,$moodledata=false) {
        global $CFG;
        $dirbase = $CFG->dirroot;
		if($moodledata){ $dirbase = $CFG->dataroot;}
       
        $list=array();
		$list['']="";
        $tdir = $dirbase . "/theme/badiumview/factory/$type";

        $files = scandir($tdir);
		
        foreach ( $files as $k => $v) {
          	$fpath="$tdir/$v";
			if($v!='.' &&  $v!='..'){
				if(is_dir($fpath)){
					$tname='factory'.$type.$v;
					$lname=$v;
					if(!$moodledata){
						$pname=get_string($tname,'theme_badiumview');
						 $pos=stripos($pname, "[[");
						 if($pos=== false){$lname=$pname;}
					}
					$list[$tname]=$lname;
				}  
			}
			
			
        }
        return $list;
    }	
}