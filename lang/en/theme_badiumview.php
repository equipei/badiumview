<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file.
 *
 * @package   theme_badiumview
 * @copyright 2016 Damyon Wiese
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// This line protects the file from being accessed by a URL directly.
defined('MOODLE_INTERNAL') || die();

// The name of the second tab in the theme settings.
$string['advancedsettings'] = 'Advanced settings';
// The backgrounds tab name.
$string['backgrounds'] = 'Backgrounds';
// The brand colour setting.
$string['brandcolor'] = 'Brand colour';
// The brand colour setting description.
$string['brandcolor_desc'] = 'The accent colour.';
// A description shown in the admin theme selector.
$string['choosereadme'] = 'Theme badiumview is a child theme of Boost. It adds the ability to upload background badiumviews.';
// Name of the settings pages.
$string['configtitle'] = 'Configurações do Badiu MView';
// Background image for dashboard page.
$string['dashboardbackgroundimage'] = 'Dashboard page background image';
// Background image for dashboard page.
$string['dashboardbackgroundimage_desc'] = 'An image that will be stretched to fill the background of the dashboard page.';
// Background image for default page.
$string['defaultbackgroundimage'] = 'Default page background image';
// Background image for default page.
$string['defaultbackgroundimage_desc'] = 'An image that will be stretched to fill the background of all pages without a more specific background image.';
// Background image for front page.
$string['frontpagebackgroundimage'] = 'Front page background image';
// Background image for front page.
$string['frontpagebackgroundimage_desc'] = 'An image that will be stretched to fill the background of the front page.';
// Name of the first settings tab.
$string['generalsettings'] = 'General settings';
// Background image for incourse page.
$string['incoursebackgroundimage'] = 'Course page background image';
// Background image for incourse page.
$string['incoursebackgroundimage_desc'] = 'An image that will be stretched to fill the background of course pages.';
// Background image for login page.
$string['loginbackgroundimage'] = 'Login page background image';
// Background image for login page.
$string['loginbackgroundimage_desc'] = 'An image that will be stretched to fill the background of the login page.';
// The name of our plugin.
$string['pluginname'] = 'Badiu Mview';
// Preset files setting.
$string['presetfiles'] = 'Additional theme preset files';
// Preset files help text.
$string['presetfiles_desc'] = 'Preset files can be used to dramatically alter the appearance of the theme. See <a href=https://docs.moodle.org/dev/Boost_Presets>Boost presets</a> for information on creating and sharing your own preset files, and see the <a href=http://moodle.net/boost>Presets repository</a> for presets that others have shared.';
// Preset setting.
$string['preset'] = 'Theme preset';
// Preset help text.
$string['preset_desc'] = 'Pick a preset to broadly change the look of the theme.';
// Raw SCSS setting.
$string['rawscss'] = 'Raw SCSS';
// Raw SCSS setting help text.
$string['rawscss_desc'] = 'Use this field to provide SCSS or CSS code which will be injected at the end of the style sheet.';
// Raw initial SCSS setting.
$string['rawscsspre'] = 'Raw initial SCSS';
// Raw initial SCSS setting help text.
$string['rawscsspre_desc'] = 'In this field you can provide initialising SCSS code, it will be injected before everything else. Most of the time you will use this setting to define variables.';
// We need to include a lang string for each block region.
$string['region-side-pre'] = 'Right';

$string['course']='Curso'; 
$string['lastaccess']='Último acesso'; 
$string['status']='Status';
$string['id']='Id';

$string['yes']='Sim'; 
$string['no']='Não'; 

$string['topicdefaulname'] = 'Tópico';
 
//course status
$string['inprogress'] = 'Em andamento';
$string['completed'] = 'Concluído';
$string['notstarted'] = 'Não iniciado';
$string['coursescompleted'] = 'Cursos concluídas ';

$string['notstartedbysection'] = 'Não liberado devido ao bloqueio de tópico';
$string['withoutprogressconfig'] = 'Sem rastreamento de conclusão';

$string['withoutenrolmessage'] = 'Você não está inscrito em nenhum curso';

$string['factorytheme'] = 'Projeto gráfico';
$string['factorythemetemplate'] = 'Projeto gráfico do site Moodle';
$string['factorythemetemplate_desc'] = 'Thema do site com estrutura de cabeçalho e rodapé e tela de login. No thema será definido CSS que determina a configuração global do Moodle';

$string['factorymy'] = 'Painel do usuário';
$string['factorymytemplate'] = 'Template do painel do usuário';
$string['factorymytemplate_desc'] = 'Trata-se da página que lista a relação dos cursos o usuário logado está inscrito. Substitui o conteúdo padrão da página my (painel de cursos) padrão do Moodle.';
$string['factorymytemplate1'] = 'Trilha de conteúdo do curso em forma de tabela';
$string['factorymytemplate2'] = 'Trilha de conteúdo do curso em forma de card';

$string['factorymytitleforstudent'] = 'Meus cursos';
$string['factorymytitleforteachear'] = 'Cursos que estou ministrando';

$string['factorymymenucurrentcourses'] = 'Cursos recentes';
$string['factorymymenupreviouscourses'] = 'Cursos anteriores';

$string['amsdiscipline'] = 'Disciplina';
$string['amscurrentdiscipline'] = 'Disciplinas atuais';
$string['amspreviousdiscipline'] = 'Disciplinas anteriores';
$string['amstododiscipline'] = 'Disciplinas a cursar';
$string['amscurriculum'] = 'Matriz curricular';

$string['factorymytemplatefitercoursebytimestart']='Exibir cursos que iniciaram a partir da data configurada';
$string['factorymytemplatefitercoursebytimestart_desc']='Cadastre data no formato YYYY/MM/DD para filtrar cursos pela data de início. Será exibido na página que lista as inscrições, apenas os cursos que iniciam a partir da data configurada. Caso a data não for configurada ou for configurada com formato errado, o filtro não será aplicado.';

$string['factorymymessageshowinhead']='Mensagem a ser exibido no início do painel';
$string['factorymymessageshowinhead_desc']='Digite uma mensagem que deve ser apresentado aos alunos e tutores no início do painel, antes da lista dos cursos.';


$string['factorycourseformat'] = 'Formato de curso';
$string['factorycourseformattemplate'] = 'Template do conteúdo do curso';
$string['factorycourseformattemplate_desc'] = 'Trata-se do formato de exibição de recursos e atividades no nível do thema que substituirá o formato do curso configurado no curso.';
$string['factorycourseformattemplate1'] = 'Trilha de conteúdo do curso em forma de tabela';
$string['factorycourseformattemplate2'] = 'Trilha de conteúdo do curso em forma de card';

$string['factorycourseformatfirsttopicaspresentation']='Usar o primeiro tópico como apresentação do curso';
$string['factorycourseformatfirsttopicaspresentation_desc']='Esta configuração exibe o conteúdo do primeiro tópico na área superior e fica sempre visível. Desta forma, o primeiro tópica não será exibido na lista de tópicos. Será usado para publicar conteúdos sobre apresentação do curso, mensagens de boas vindas ou direcionamento pedagógico do curso.';

/*
$string['factorytheme'] = 'Cabeçalho e Rodapé';

$string['factoryheader'] = 'Cabeçalho do site';
$string['factoryheader_desc'] = 'Escolha a opção do cabeçalho do site Moodle';

$string['factoryfooter'] = 'Cabeçalho do site';
$string['factoryfooter_desc'] = 'Escolha a opção do redapé do site Moodle';
*/
$string['countrecord']='Total de registro';
$string['requiredfield']='Campo obrigatório';

$string['id'] = 'Id';
$string['key'] = 'Chave';
$string['value'] = 'Valor';
$string['level'] = 'Nível';
$string['description'] = 'Descrição';
$string['addnew'] = 'Cadastrar novo';
$string['dconfig'] = 'Configuração';
$string['general'] = 'Geral';
$string['otherconfig'] = 'Outras configurações';
$string['save'] = 'Salvar';
$string['view'] = 'visualizar';
$string['edit'] = 'editar';
$string['remove'] = 'excluir';

$string['addsuccess'] = 'Cadastro efetuado com sucesso';
$string['editsuccess'] = 'Registro com id {$a} foi  atualizado com sucesso';
$string['removesuccess'] = 'Registro <em>{$a}</em> excluído com sucesso<br />';
$string['removeconfirm'] = 'Tem certeza que deseja excluir o registo <em>{$a}</em> da base de dados?';


$string['configitemmanager'] = 'Gerenciar configuração';
$string['configitemadd'] = 'Adicionar configuração';
$string['configitemedit'] = 'Alterar configuração';
$string['configitemremove'] = 'Exlcuir configuração';

$string['configdtype'] = 'Tipo de conteúdo';
$string['configdtypefile'] = 'Arquivo';
$string['configdtypehtml'] = 'HTML';
$string['configdtypetext'] = 'Texto';
$string['configdtypejson'] = 'JSON';

$string['context'] = 'Contexto';
$string['contextsystem'] = 'Contexto do sistema';
$string['contextcoursecategory'] = 'Contexto da categoria de curso';
$string['contextcourse'] = 'Contexto do curso';

$string['instanceid'] = 'Id do contexto';
$string['enableblockscontrolshow'] = 'Habilitar controle para abrir e ocultar lista de blocos';
$string['enableblockscontrolshow_desc'] = 'Caso  um ou mais blocos estiver habilitado, o usuário terá opção ocultar ou exibir a coluna mostra os blocos';

$string['singlenerolredirecttocourse'] = 'Redirecionara usuário diretamente para o ambiente do curso caso estiver inscrito em apenas um curso';
$string['singlenerolredirecttocourse_desc'] = 'Se estiver habilitado, o usuário que estiver inscrico em apenas um curso, não terá opção de visualizar painel com os cursos que está inscrito para escolher em qual acessar. Essa tela será pulida, o usuário será automaticamente redirecionado para o ambiente do curso em que estiver inscrito';


