<?php
class theme_badiumview_course_rolelib {
	
	 function __construct() { 
		
    }
 
 
 function hasStudentProfile($userid) {
	 global $DB, $CFG;
     if(empty($userid)){ return 0;}
	  $rolesid=$this->getStudentPprofile();
	 if(!empty($rolesid)){
		 $wsql="";
		 $pos=stripos($rolesid, ",");
		 if($pos=== false){ $wsql=" AND rs.rolid = $rolesid";}
		 else { $wsql=" AND rs.rolid IN ($rolesid) ";}
		 $sql = "SELECT COUNT(rs.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id  WHERE e.contextlevel=50 AND rs.userid=$userid $wsql";
		 $r = $DB->get_record_sql($sql);
		return $r->countrecord;
	 }
      $sql = "SELECT COUNT(rs.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}role r ON r.id=rs.roleid WHERE e.contextlevel=50 AND r.shortname ='student' AND rs.userid=$userid";
	 $r = $DB->get_record_sql($sql);
	return $r->countrecord;
}
	
function  hasTeachearProfile($userid) {
	 global $DB, $CFG;
     if(empty($userid)){ return 0;}
	 $sql = "SELECT COUNT(rs.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}role r ON r.id=rs.roleid WHERE e.contextlevel=50  AND rs.userid=$userid AND r.shortname IN ('eacher','editingteacher','manager') ";
     $r = $DB->get_record_sql($sql);
     return $r->countrecord;
}


function getStudentPprofile() {
	global $DB, $CFG;
	$sql = "SELECT value FROM {$CFG->prefix}config WHERE name = 'gradebookroles' ";
	$r = $DB->get_record_sql($sql);
	return $r->value;
 }	
}

?>
