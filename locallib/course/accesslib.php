<?php

class theme_badiumview_course_accesslib {

	function get_lastaccess_ativities($courseid,$userid) {
       
        global $DB;
        global $CFG;
        if(empty($userid)){return null;}
		if(empty($courseid)){return null;}
        $sql = "SELECT contextinstanceid AS cmid,MAX(timecreated) AS lastaccess FROM {$CFG->prefix}logstore_standard_log WHERE courseid=$courseid AND userid=$userid AND component !='core' GROUP BY contextinstanceid ORDER BY MAX(timecreated) DESC";
		 $rows = $DB->get_records_sql($sql);
        return $rows;
    }	
	function get_lastaccess_course($courseid,$userid) {
       
        global $DB;
        global $CFG;
        if(empty($userid)){return null;}
		if(empty($courseid)){return null;}
        $sql = "SELECT timeaccess FROM  {$CFG->prefix}user_lastaccess WHERE userid=$userid AND courseid=$courseid";
		$row = $DB->get_record_sql($sql);
		if(!empty($row)){return $row->timeaccess;}
        return null;
    }	  
	function get_ativityid_lastaccessed($lislastaccessativities) {
		$lastactivityidaccessed=null;
		 if(!is_array($lislastaccessativities)){return null;}
		$lastactivityaccessed= array_shift($lislastaccessativities);
		if(isset($lastactivityaccessed->cmid)){$lastactivityidaccessed=$lastactivityaccessed->cmid;}
	    return $lastactivityidaccessed;
    }

	function get_lastaccess_ativities_array_id_key($lislastaccessativities) {
		$newlist=array();
		if(!is_array($lislastaccessativities)){return $newlist;}
		foreach ($lislastaccessativities as $row) {
			if(isset($row->cmid)){
				$cmid=$row->cmid;
				$lastaccess=$row->lastaccess;
				$newlist[$cmid]=$lastaccess;
			}
			
		}
		return $newlist;
	}	
}

?>
