<?php

require_once ($CFG->libdir . '/filelib.php');
require_once ($CFG->libdir . '/completionlib.php');
require_once ($CFG->libdir . '/modinfolib.php');
require_once("$CFG->dirroot/course/renderer.php");

require_once($CFG->dirroot.'/course/format/renderer.php');
require_once("$CFG->dirroot/theme/badiumview/locallib/utildata.php");
class theme_badiumview_course_contentlib {

    function get_full_by_course($courseid,$coursetopic=null) {

        global $DB;
        global $PAGE;
        $list = array();
        $course = $DB->get_record('course', array('id' => $courseid));
        $renderer = new core_course_renderer($PAGE, null);
        $modinfo = get_fast_modinfo($course);
        $displayoptions = array();
        $sectionreturn = null;

   
        $list['topicos'] = $modinfo->sections;
 
       $completioninfo = new completion_info($course);

        //get atividadeshtmliconedit
        $atividadeshtmliconedit = array();
        $ativitieshtml = array();
        $addcontroll = array();
        global $USER;
        //$USER->editing = 1;
        foreach ($list['topicos'] as $k => $v) {
            if($coursetopic >=0 && $k ==$coursetopic){
               
                foreach ($v as $a) {
                    $mod = $modinfo->cms[$a];

                    //get htmectivities

                    $activityhml = $renderer->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions);
                    //$controll=  $renderer->course_section_add_cm_control($course, $section, $sectionreturn = null, $displayoptions = array());

                    $ativitieshtml[$a] = $activityhml;
                    //get edit eicon
                    $editactions = course_get_cm_edit_actions($mod, $mod->indent, $sectionreturn);
                    $editicone = $renderer->course_section_cm_edit_actions($editactions, $mod, $displayoptions);
                    $atividadeshtmliconedit[$a] = $editicone;
                }
            }else  if($coursetopic===null){
               
                 foreach ($v as $a) {
                    $mod = $modinfo->cms[$a];

                    //get htmectivities

                    $activityhml = $renderer->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions);
                    //$controll=  $renderer->course_section_add_cm_control($course, $section, $sectionreturn = null, $displayoptions = array());

                    $ativitieshtml[$a] = $activityhml;
                    //get edit eicon
                    $editactions = course_get_cm_edit_actions($mod, $mod->indent, $sectionreturn);
                    $editicone = $renderer->course_section_cm_edit_actions($editactions, $mod, $displayoptions);
                    $atividadeshtmliconedit[$a] = $editicone;
                }
            }
            
            $addcontroll[] = $renderer->course_section_add_cm_control($course, 0);


            $list['atividadeshtmliconedit'] = $atividadeshtmliconedit;
            $list['ativitieshtml'] = $ativitieshtml;
            $list['addcontroll'] = $addcontroll;
        }

        return $list;
    }   

 
     /**
     * get activities enables in course.
     *
     * @param $courseid
     * @param null $coursetopic
     * @return array
     * @throws dml_exception
     * @throws moodle_exception
     */
    function get_enable_ativities($courseid, $coursetopic = null) {
        $listativities = $this->get_full_by_course($courseid, $coursetopic);
        $listenabled   = array();

        if (isset($listativities['ativitieshtml'])) {
            $listativities=$listativities['ativitieshtml'];

            foreach ($listativities as $key => $value) {
                if (!empty($value)) {
                    $listenabled[$key] = $key;
                }
            }
        }
        return $listenabled;
    }
    function get_list_item($courseid,$coursetopic=null,$modediting=true) {

        global $DB;
        global $PAGE;
        $list = array();
        $course = $DB->get_record('course', array('id' => $courseid));
        $renderer = new core_course_renderer($PAGE, null);
        $modinfo = get_fast_modinfo($course);
        $displayoptions = array();
        $sectionreturn = null;


        $list['topicos'] = $modinfo->sections;

        $completioninfo = new completion_info($course);

       
        $ativitieshtml = array();
    
        global $USER;
        if(!$modediting){$USER->editing = false;}
        foreach ($list['topicos'] as $k => $v) {
            if($coursetopic >=0 && $k ==$coursetopic){
                foreach ($v as $a) {
                    $mod = $modinfo->cms[$a];

                    //get htmectivities
                    //code of course_section_cm_list_item in MOODLE_DIR_INSTALL/renderer.php
                    $activityhml = $renderer->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions);
                    $modclasses = 'activity ' . $mod->modname . ' modtype_' . $mod->modname . ' ' . $mod->extraclasses;
                    $output = html_writer::tag('li', $activityhml, array('class' => $modclasses, 'id' => 'module-' . $mod->id));
                    $ativitieshtml[$a] = $activityhml;
              }

            }else  if($coursetopic===null){
                foreach ($v as $a) {
                    $mod = $modinfo->cms[$a];

                    //get htmectivities
                    //code of course_section_cm_list_item in MOODLE_DIR_INSTALL/renderer.php
                    $activityhml = $renderer->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions);
                    $modclasses = 'activity ' . $mod->modname . ' modtype_' . $mod->modname . ' ' . $mod->extraclasses;
                    $output = html_writer::tag('li', $activityhml, array('class' => $modclasses, 'id' => 'module-' . $mod->id));
                    $ativitieshtml[$a] = $activityhml;
              }
            }
            
          
        }

        return $ativitieshtml;
    }      
 function get_list($courseid,$section=null) {
         if(empty($courseid)){return null;}
        global $DB;
        global $CFG;
        $wsql="";
        if($section > 0 ){$wsql=" AND s.section=$section";}
        $sql = "SELECT cm.id,m.name AS module,cm.instance,cm.idnumber,s.id AS topicid,s.section,s.sequence AS activitysequence,cm.visible,cm.visible,cm.visibleold,cm.groupmode,cm.completion,cm.completiongradeitemnumber,cm.completionview,cm.added FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}course_sections s ON s.id=cm.section WHERE cm.deletioninprogress=0 AND s.course=cm.course AND cm.course=$courseid  AND cm.visibleold=1  AND cm.visible=1 AND cm.deletioninprogress=0 $wsql ORDER BY s.section ";
        $listcontent = $DB->get_records_sql($sql); 
        
        $listcontent=$this->add_info_tolist($listcontent,$courseid,$section);
        $listcontent=$this->oder_list($listcontent);
        return $listcontent;
    }
   
 /**
     * Get activities completed by courseid.
     *
     * @param $userid
     * @param $courseid
     * @return array
     * @throws dml_exception
     */
    public function get_list_completed($userid, $courseid) {
        global $DB;
		if(empty($userid)){return null;}
		if(empty($courseid)){return null;}
        $sql = "SELECT cm.id FROM {course_modules_completion} cmp INNER JOIN {course_modules} cm ON cmp.coursemoduleid = cm.id  WHERE cm.completion > 0 AND cmp.completionstate > 0 AND cmp.completionstate < 3 AND cm.deletioninprogress = 0 AND cmp.userid = :userid AND cm.course = :courseid";
        $rows = $DB->get_records_sql($sql, ['userid' => $userid, 'courseid' => $courseid]);
        $newlist = array();
        foreach ($rows as $row) {
            $newlist[$row->id] = $row->id;
        }
        return $newlist;
    }   
   private  function add_info_tolist($listcontent,$courseid,$section=null) {
       if(empty($listcontent)) {return null;}
        $listcontentnew=array();
        $listplugins=array();
        foreach ($listcontent as $d) {
               $name=$d->module;
              $listplugins[$name]=  $name;
         }
       $listcontentname=array();
       foreach ($listplugins as $plugin) {
           $instacelist=$this->get_name($plugin,$courseid);
           foreach ($instacelist as $linfo) {
               $id=$linfo->id;
               $key="$plugin/$id";
               $listcontentname[$key]=$linfo;
            }
           
       }
       foreach ( $listcontent as $lcontent) {
           $instance=$lcontent->instance;
           $module=$lcontent->module;
           $key="$module/$instance";
           if(array_key_exists($key,$listcontentname)){
               $info=$listcontentname[$key];
               $lcontent->name=$info->name;
               $lcontent->intro ='';
			  if(isset($info->intro)){  $lcontent->intro =$info->intro;}
           }
          array_push($listcontentnew,$lcontent); 
       }
     return $listcontentnew;
    }
    
   private  function oder_list($listcontent) {
          if(empty($listcontent)) {return null;}
          $newlistcontent=array();
          $listbykey=array();
          $sectionsequences=array();
          foreach ($listcontent as $lc) {
             $key= $lc->section."/".$lc->id;
             $listbykey[$key]=$lc;
             
             $seq=array();
             $pos=stripos($lc->activitysequence, ",");
              if($pos=== false){
                 $seq=array($lc->activitysequence); 
              }else{
                  $seq=explode(",",$lc->activitysequence);
              }
             $sectionsequences[$lc->section]=$seq;
          }
          foreach ( $sectionsequences as $skey => $listidativity) {
              foreach ($listidativity as $ativityid) {
                  $ativkey=$skey."/".$ativityid;
                  if (array_key_exists($ativkey,$listbykey)){
                      $itemativity=$listbykey[$ativkey];
                     array_push($newlistcontent,$itemativity);  
                  }
              }
            }
        return   $newlistcontent;    
    }
    
  private function get_name($plugin,$courseid) {
        global $DB;
        global $CFG;
        $sql="SELECT * FROM {$CFG->prefix}$plugin WHERE course=$courseid";
        $rows = $DB->get_records_sql($sql);
        return $rows;
    }
    
    function get_plugins() {
        global $DB;
        global $CFG;
        $wsql="";
        if($section > 0 ){$wsql=" AND s.section=$section";}
        $sql = "SELECT cm.id,m.name,cm.instance,cm.idnumber,s.id AS topicid,s.section,cm.visible,cm.visible,cm.visibleold,cm.groupmode,cm.completion,cm.completiongradeitemnumber,cm.completionview,cm.added FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module INNER JOIN {$CFG->prefix}course_sections s ON s.id=cm.section WHERE  s.course=cm.course AND cm.course=$courseid $wsql ";

       
        $rows = $DB->get_records_sql($sql);
        return $rows;
    }
    
     function get_topics($param) {
         $util=new theme_badiumview_utildata();
        global $DB;
        global $CFG;
        $courseid=$util->getVaueOfArray($param,'courseid');
        $visible=$util->getVaueOfArray($param,'visible');
		$section=$util->getVaueOfArray($param,'section');
        if(empty($visible)){$visible=1;}
        $wsql="";
        if($section > 0 ){$wsql=" AND s.section=$section";}
		//REVIEW  AND ( summaryformat > 0  OR name IS NOT NULL) 
        $sql = "SELECT id,name,section,summary FROM {$CFG->prefix}course_sections WHERE course=$courseid AND visible=$visible AND ( summaryformat > 0  OR name IS NOT NULL) ORDER BY section ";

        $rows = $DB->get_records_sql($sql);
        return $rows;
    }
    

    function get_topic_info_by_section($courseid,$section) {
       
        global $DB;
        global $CFG;
        
        $sql = "SELECT s.id,s.name,s.section,c.fullname AS course,c.shortname AS courseshortname,ct.name AS category,ct.id AS  categoryid  FROM {$CFG->prefix}course_sections s INNER JOIN {$CFG->prefix}course c   ON c.id=s.course INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id WHERE s.course=$courseid AND s.section= $section";

        $row = $DB->get_record_sql($sql);
        return $row;
    }
	 
	 public function add_topic_config($param) { 
          $newlist=array();
		  $util=new theme_badiumview_utildata();
		  $courseid=$util->getVaueOfArray($param,'courseid');
		  $topicwslist=$util->getVaueOfArray($param,'list');
		
		  $firsttopicaspresentation=$util->getVaueOfArray($param,'firsttopicaspresentation');
		  
		  $course=new stdClass();
		  $course->id=$courseid; 
		  $modinfo = get_fast_modinfo($course);
		  $sections = $modinfo->get_section_info_all();
		 
          foreach ($topicwslist as $row) {
			  $thissection = $util->getVaueOfArray($sections,$row->section);
             // $id=$row->section;
			  //$sectionid=$row->id;
              $name=$row->name;
			  $summary=$row->summary;
			  
              if(empty($name)){$name=get_string('topicdefaulname', 'theme_badiumview').' '.$row->section;}
			  if($firsttopicaspresentation && $row->section==0){ $name=$row->name;}
			  
			  $tparam=array('id'=>$row->id,'name'=>$name,'summary'=>$summary,'section'=>$row->section,'visible'=>$thissection->visible,'uservisible'=>$thissection->uservisible,'available'=>$thissection->available,'availableinfo'=>$thissection->availableinfo);
			  array_push($newlist,$tparam); 
              
          }
         return $newlist;
      }
	
	function get_ativities_access_by_user($courseid,$userid) {
       
        global $DB;
        global $CFG;
        if(empty($userid)){return null;}
		if(empty($courseid)){return null;}
        $sql = "SELECT contextinstanceid AS cmid,COUNT(id) AS countaccess FROM {$CFG->prefix}logstore_standard_log WHERE courseid=$courseid AND userid=$userid GROUP BY contextinstanceid ";
		 $rows = $DB->get_records_sql($sql);
        return $rows;
    }	
	  
	  
}

?>
