<?php

class theme_badiumview_course_coursecatlib {
	
	 function __construct() { 
	
    }
  
  
  function addNameToList($rows) {
	  if(empty($rows) || !is_array($rows)){return $rows;}
	
      $displaylist=$this->getParents();
        $newlist=array();
        foreach ($rows as $row) {
                $id=$row->category;
				$row->categoryname='';
                if (array_key_exists($id,$displaylist)){
                    $row->categoryname=$displaylist[$id];
                 }
				 array_push($newlist,$row);
            } 
        return $newlist;
        
    }
	
	  function getParentsByid($id) {
        global $CFG;
		$listparent=null;
		if($CFG->version < 2018120310){
			require_once("$CFG->dirroot/lib/coursecatlib.php");
			 $listparent=coursecat::get($id)->get_parents();
		}else{
			$listparent=core_course_category::get($id)->get_parents(); 
		}
		return $listparent;
     }
     function getParents() {
        global $CFG;
		$displaylist=array();
		if($CFG->version < 2018120310){
			 require_once("$CFG->dirroot/lib/coursecatlib.php");
			$displaylist = coursecat::make_categories_list();
		}else{
			 $displaylist = core_course_category::make_categories_list();
		}
		return $displaylist;	
     }
}

?>
