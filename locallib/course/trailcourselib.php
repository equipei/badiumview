<?php
require_once("$CFG->dirroot/theme/badiumview/locallib/baselib.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/course/coursecatlib.php");
class theme_badiumview_course_trailcourselib extends theme_badiumview_baselib {
	
	 function __construct() { 
		parent::__construct();
    }
  function isInteger($value) {
	  if(is_int($value)){return true;}
	  else return false;
  }
 function getEnrolsWsqlparam($param) {
		$coursetimestart = $this->getUtildata()->getVaueOfArray($param,'coursetimestart');
		$coursetimestartoperator = $this->getUtildata()->getVaueOfArray($param,'coursetimestartoperator');
		if(empty($coursetimestartoperator)) {$coursetimestartoperator = " >= ";}
		$coursevisible = $this->getUtildata()->getVaueOfArray($param,'coursevisible');
		$component = $this->getUtildata()->getVaueOfArray($param,'component');
		$wsql=""; 
		
		
		if($this->isInteger($coursevisible)){ $wsql.=" AND c.visible=$coursevisible";}
		if(!empty($component)){ $wsql.=" rs.component='".$component."'";}
		if($this->isInteger($coursetimestart)){ $wsql.=" AND c.startdate $coursetimestartoperator $coursetimestart ";}
		
		$enrolstatus = $this->getUtildata()->getVaueOfArray($param,'enrolstatus');
		if($this->isInteger($enrolstatus)){ $wsql.=" AND ue.status = $enrolstatus ";}
		
		$enrolmethodstatus = $this->getUtildata()->getVaueOfArray($param,'enrolmethodstatus');
		if($this->isInteger($enrolmethodstatus)){ $wsql.=" AND en.status = $enrolmethodstatus ";}
		 
		$enroltimevalidate = $this->getUtildata()->getVaueOfArray($param,'enroltimevalidate');
		$now=time();
		if($this->isInteger($enroltimevalidate) && intval($enroltimevalidate)==1){ $wsql.=" AND (ue.timestart IS NULL OR ue.timestart = 0 OR ue.timestart <= $now ) AND (ue.timeend IS NULL OR ue.timeend = 0 OR ue.timeend >= $now ) ";}
		if($enroltimevalidate!=null && $enroltimevalidate!='' && intval($enroltimevalidate) ==0){ $wsql.="  AND ((ue.timestart > 0 AND ue.timestart > $now )  OR (ue.timeend > 0 AND ue.timeend < $now ) )";}
		
		return $wsql;
	}
	function getEnrolsCount($param) {
		$userid   = $this->getUtildata()->getVaueOfArray($param,'userid');
		if(empty($userid)){return null;}
		$wsql=$this->getEnrolsWsqlparam($param);
		global $DB;
        global $CFG;
        $sql = "SELECT COUNT(DISTINCT  c.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}course c ON c.id=e.instanceid INNER JOIN {$CFG->prefix}course_categories ct ON ct.id=c.category INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON (en.id=ue.enrolid AND rs.userid=ue.userid)  LEFT JOIN {$CFG->prefix}course_completions p ON ( p.course=c.id AND p.userid=rs.userid)   LEFT JOIN {$CFG->prefix}user_lastaccess ul ON (c.id=ul.courseid AND rs.userid=ul.userid) WHERE e.contextlevel=50 AND rs.userid=$userid  $wsql ";
		$row = $DB->get_record_sql($sql);
		//echo $sql;exit;
		if(!empty($row)){return $row->countrecord;}
        return $row;
    }	
	
	function getEnrols($param) {
		$userid   = $this->getUtildata()->getVaueOfArray($param,'userid');
		if(empty($userid)){return null;}
        $wsql=$this->getEnrolsWsqlparam($param);
        global $DB;
        global $CFG;
        $sql = "SELECT DISTINCT  c.id, c.fullname,p.timecompleted,ul.timeaccess AS timelastaccess,c.category, (SELECT COUNT(DISTINCT cm1.id) AS countactivityanebleprogress FROM {$CFG->prefix}course_modules cm1 WHERE cm1.course=c.id AND cm1.completion > 0  AND cm1.visibleold=1 AND cm1.deletioninprogress=0) AS countactivityanebleprogress, (SELECT COUNT(DISTINCT cmp2.id) AS countactivitycompleted FROM {$CFG->prefix}course_modules_completion cmp2 INNER JOIN {$CFG->prefix}course_modules cm2 ON cmp2.coursemoduleid = cm2.id WHERE  cm2.course=c.id AND cmp2.userid=rs.userid AND cm2.completion > 0 AND cmp2.completionstate > 0 AND cmp2.completionstate < 3 AND cm2.visibleold=1 AND cm2.deletioninprogress=0) AS countactivitycompleted FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}course c ON c.id=e.instanceid INNER JOIN {$CFG->prefix}course_categories ct ON ct.id=c.category INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON (en.id=ue.enrolid AND rs.userid=ue.userid) LEFT JOIN {$CFG->prefix}course_completions p ON ( p.course=c.id AND p.userid=rs.userid)   LEFT JOIN {$CFG->prefix}user_lastaccess ul ON (c.id=ul.courseid AND rs.userid=ul.userid) WHERE e.contextlevel=50 AND rs.userid=$userid  $wsql ORDER BY ul.timeaccess DESC";
		$rows = $DB->get_records_sql($sql);
		
		$showcoursecategorypath=$this->getUtildata()->getVaueOfArray($param,'showcoursecategorypath');
		if(!empty($rows) && $showcoursecategorypath){
			$coursecat=new theme_badiumview_course_coursecatlib();
			$rows=$coursecat->addNameToList($rows);
		}
		
        return $rows;
    }	
	
	function getEnrolsCountCompletationEnabled($param) {
		
		$userid   = $this->getUtildata()->getVaueOfArray($param,'userid');
		if(empty($userid)){return null;}
        $wsql=$this->getEnrolsWsqlparam($param);
       
        global $DB;
        global $CFG;
        $sql = "SELECT COUNT(DISTINCT  c.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}course c ON c.id=e.instanceid INNER JOIN {$CFG->prefix}course_categories ct ON ct.id=c.category INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON (en.id=ue.enrolid AND rs.userid=ue.userid) INNER JOIN {$CFG->prefix}course_completion_criteria ccc ON c.id=ccc.course  LEFT JOIN {$CFG->prefix}course_completions p ON ( p.course=c.id AND p.userid=rs.userid) WHERE e.contextlevel=50  AND rs.userid=$userid  $wsql ";
		$row = $DB->get_record_sql($sql);
		if(!empty($row)){return $row->countrecord;}
        return $row;
    }
	function getEnrolsCountCompletationEnd($param) {
		
		$userid   = $this->getUtildata()->getVaueOfArray($param,'userid');
		if(empty($userid)){return null;}
        $wsql=$this->getEnrolsWsqlparam($param);
        global $DB;
        global $CFG;
        $sql = "SELECT COUNT(DISTINCT  c.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}course c ON c.id=e.instanceid INNER JOIN {$CFG->prefix}course_categories ct ON ct.id=c.category INNER JOIN {$CFG->prefix}enrol en ON  e.instanceid=en.courseid  INNER JOIN {$CFG->prefix}user_enrolments ue ON (en.id=ue.enrolid AND rs.userid=ue.userid) INNER JOIN {$CFG->prefix}course_completion_criteria ccc ON c.id=ccc.course   LEFT JOIN {$CFG->prefix}course_completions p ON ( p.course=c.id AND p.userid=rs.userid) WHERE e.contextlevel=50 AND p.timecompleted > 0 AND rs.userid=$userid  $wsql ";
		$row = $DB->get_record_sql($sql);
		if(!empty($row)){return $row->countrecord;}
        return $row;
    }
	
		
}

?>
