<?php
require_once("$CFG->dirroot/theme/badiumview/locallib/utildata.php");
require_once("$CFG->dirroot/theme/badiumview/app/config/item/dblib.php");
class theme_badiumview_baselib {

	private $utildata;
	private $mconfig;
	private $bparam;
	function __construct($bparam=null) {
		$this->utildata=new theme_badiumview_utildata();
		$this->mconfig=new theme_badiumview_app_config_item_dblib();
		$this->bparam=$bparam;
    }
	
	
	
    public function getUtildata() {
        return $this->utildata;
    }

    public function setUtildata($utildata) {
        $this->utildata = $utildata;
    }
	
	    public function getMconfig() {
        return $this->mconfig;
    }

    public function setMconfig($mconfig) {
        $this->mconfig = $mconfig;
    }
	
	public function getBparam() {
        return $this->bparam;
    }

    public function setBparam($bparam) {
        $this->bparam = $bparam;
    }
}

?>
