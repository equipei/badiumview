<?php
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/netlib.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/maccess.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/factorycontent.php");

class theme_badiumview_badiunetclient_locallib {
  private $content = null;
  private $paramstart=array();
  private $paramdefault=array();
  function __construct() {
   
  }
  
  function exec($param=array()) {
      $factorycontent=new local_badiunet_factorycontent('notformated',$param);
      $factorycontent->setParamsytemstart($this->getParamstart());
      $factorycontent->setParamsytemdefault($this->getParamdefault());
      $factorycontent->init();
      $response=$factorycontent->getContent();

      if(!$factorycontent->getUtil()->isResponseError($response)){
          $message=$factorycontent->getUtil()->getVlueOfArray($response, 'message');
    
          $message=$factorycontent->getTcript()->decode($message);   
          $message=$this->replaceUrl($message);
          $message=$factorycontent->getUtil()->getJson($message, true);
          
          $response['message']=$message;

      }
        $this->content =$response;
}

function replaceUrl($content,$url=null){ 
    global $CFG; 
    $urlbase=$CFG->httpswwwroot.'/local/badiunetclient/index.php';
    $content=str_replace("BADIU_CORE_SERVICE_CLIENTE_URLBASE",$urlbase,$content);
    if(!empty($url)){$content=str_replace("BADIU_CORE_SERVICE_CLIENTE_CURRENT_URL",$url,$content);}
    return $content;
}

function addParamstart($key,$value) {
  $this->paramstart[$key]=$value;
}
function addParamdefault($key,$value) {
  $this->paramdefault[$key]=$value;
}
  function getContent() {
    return $this->content;
}

function setContent($content) {
    $this->content = $content;
}

function getParamstart() {
  return $this->paramstart;
}

function setParamstart($paramstart) {
  $this->paramstart = $paramstart;
}

function getParamdefault() {
  return $this->paramdefault;
}

function setParamdefault($paramdefault) {
  $this->paramdefault = $paramdefault;
}
}
?>
