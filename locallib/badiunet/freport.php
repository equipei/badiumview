<?php
require_once("$CFG->dirroot/local/badiunet/lib/utildata.php");

class theme_badiumview_badiunetclient_freport {
  private $content = null;
  private $utildata = null;
  
  function __construct($content) {
    $this->content=$content;
    $this->utildata=new local_badiunet_utildata();
  }
  

  function makeTable() {
     $table        = new html_table();
     $columns=$this->getUtildata()->getVaueOfArray($this->getContent(), 'message.content.badiu_table1.columns', true);
     $rows=$this->getUtildata()->getVaueOfArray($this->getContent(), 'message.content.badiu_table1.rows', true);
     
     $rowsviews=array();
     if(is_array($rows) && is_array($columns)){
        foreach ($rows as $row){
          $newrow=array();
          foreach ($columns as $kc => $vc){
             $rkvalue=$this->getUtildata()->getVaueOfArray($row, $kc);
             $newrow[$kc]=$rkvalue;
          }
          array_push($rowsviews,$newrow);
       }
     }
     
    
     
     $table->head =$columns;
     $table->data=$rowsviews;
     return html_writer::table($table);
  }

  function makeVMenu() {
    $links=$this->getUtildata()->getVaueOfArray($this->getContent(), 'message.links.0', true);
   // $categories=$this->getUtildata()->getVaueOfArray($this->getContent(), 'message.links.1', true);
//print_r($categories);

    $html=" <ul class=\"nav flex-column\">";
    if(is_array($links)){
        $cont=0;
        foreach ($links as $row){

            $type= $this->getUtildata()->getVaueOfArray($row, 'type');
            $position= $this->getUtildata()->getVaueOfArray($row, 'position');
            $url= $this->getUtildata()->getVaueOfArray($row, 'link.url',true);
            $name= $this->getUtildata()->getVaueOfArray($row, 'link.name',true);

            if($type=="link" && $position=="menu"){
                $html.="<li class=\"nav-item\">";
                  $html.="<a class=\"nav-link \" href=\"$url\">$name</a>";
                $html.="</li>";
            }else if($type=="category" && $position=="menu"){
              $name= $this->getUtildata()->getVaueOfArray($row, 'name');
              $items=$this->getUtildata()->getVaueOfArray($row, 'items');

              $html.="<li class=\"nav-item dropdown\">";
              $html.="<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"badiuclientnavbar$cont\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">$name</a>";
               $html.="<div class=\"dropdown-menu\" aria-labelledby=\"badiuclientnavbar$cont\">";
                if(is_array($items)){
                  foreach ($items as $rowlink){
                      $siurl= $this->getUtildata()->getVaueOfArray($rowlink, 'link.url',true);
                      $siname= $this->getUtildata()->getVaueOfArray($rowlink, 'link.name',true);
                       $html.="<a class=\"dropdown-item \" href=\"$siurl\">$siname</a>";
                  }
                }
                $html.=" </div>";
                $html.="</li>";
            }
            }
            $cont++;
        }
        
    


  
    $html.="</ul>";
    return $html;
  }

  function makeHMenu() {
    global $PAGE;
    global $CFG;
    $PAGE->navbar->add("Cliente Badiu.net", new moodle_url($CFG->httpswwwroot.'/local/badiunetclient/index.php'));
    $links=$this->getUtildata()->getVaueOfArray($this->getContent(), 'message.links.1', true);
    if(is_array($links)){
      $cont=0;
        foreach ($links as $row){
         
            $type= $this->getUtildata()->getVaueOfArray($row, 'type');
            $position= $this->getUtildata()->getVaueOfArray($row, 'position');
            $url= $this->getUtildata()->getVaueOfArray($row, 'link.url',true);
            $name= $this->getUtildata()->getVaueOfArray($row, 'link.name',true);

            if($type=="link" && $position=="navbar"){
              $cont++;
              if($cont>3){$PAGE->navbar->add($name, new moodle_url($url));}
             
        }
    }
    
  
  }

} 
function getContent() {
    return $this->content;
}

function setContent($content) {
    $this->content = $content;
}
function getUtildata() {
  return $this->utildata;
}

function setUtildata($utildata) {
  $this->utildata = $utildata;
}
}
?>
