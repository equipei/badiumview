<?php

 class theme_badiumview_pluginconfig  {
 	
      /**
     * @var string
     */
    private $name;
 	
    function __construct($name='theme_badiumview') {
       $this->name=$name;
    }
  
    function getAllAsArray() {
         global $CFG,$DB;
       $sql="SELECT name,value FROM {$CFG->prefix}config_plugins WHERE  plugin ='".$this->getName()."'";
        $rows=$DB->get_records_sql($sql);
        $list=array();
       if(!empty($rows)){
            foreach ($rows as $row) {
                  $list[$row->name]=$row->value;
            }
      }
      return $list;
    }   
	
	 function getValue($key) {
       global $CFG,$DB;
       $sql="SELECT value FROM {$CFG->prefix}config_plugins WHERE  plugin ='".$this->getName()."' AND name= '".$key."'";
       $row=$DB->get_record_sql($sql);
	   
	   if(!empty($row)) return $row->value;
       return null;
    } 
    function save($key,$value) {
          global $DB;
        $dto=new stdClass;
        $dto->plugin=$this->getName();
        $dto->name=$key;
        $dto->value=$value;
        return $DB->insert_record('config_plugins', $dto);
    } 
     function edit($key,$value) {
         
          global $DB;
        $dto=new stdClass;
        $dto->id=$this->get_id($key);
        $dto->plugin=$this->getName();
        $dto->name=$key;
        $dto->value=$value;
        return $DB->update_record('config_plugins', $dto);
    } 
    
    function add($key,$value) {
			if($value==NULL){$value= " ";}
           if($this->exist($key)){       
                 $this->edit($key,$value) ;   
           }else{
                $this->save($key,$value);
           }
      }
     function exist($key) {
       global $CFG,$DB;
       $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}config_plugins WHERE  plugin='".$this->getName()."' AND name='".$key."'";
       $r=$DB->get_record_sql($sql);
        return $r->countrecord;
    }
    function existkeyvalue($key,$value) {
       global $CFG,$DB;
       $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}config_plugins WHERE  plugin='".$this->getName()."' AND name='".$key."' AND value='".$value."'";
       $r=$DB->get_record_sql($sql);
        return $r->countrecord;
    }
    
     function get_id($key) {
          global $CFG,$DB;
       $sql="SELECT id FROM {$CFG->prefix}config_plugins WHERE  plugin='".$this->getName()."' AND name='".$key."'";
       $r=$DB->get_record_sql($sql);
        return $r->id;
    } 
    
    function getKey($value) {
          global $CFG,$DB;
       $sql="SELECT MAX(name) AS name FROM {$CFG->prefix}config_plugins WHERE  plugin='".$this->getName()."' AND value='".$value."'";
       $r=$DB->get_record_sql($sql);
        return $r->name;
    } 
    
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }


 }