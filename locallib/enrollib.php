<?php

class theme_badiumview_enrollib {
	
    function is_admin() {
        if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
        return true;
        }
        return false; 
    }
	
	function is_student_course($courseid, $userid) {
		global $DB, $CFG;
		$rolesid = $this->get_student_profile();
		if (empty($rolesid)) {
			return false;
		}
		$sql = "SELECT COUNT(rs.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id WHERE e.contextlevel=50 AND e.instanceid=$courseid AND rs.userid=$userid AND rs.roleid IN ($rolesid)";
		$r = $DB->get_record_sql($sql);
		return $r->countrecord;
	}
 
	function get_student_profile() {
		global $DB, $CFG;
		$sql = "SELECT value FROM {$CFG->prefix}config WHERE name = 'gradebookroles' ";
		$r = $DB->get_record_sql($sql);
		return $r->value;
 }
 
 function is_student() {
    global $DB, $CFG,$USER;
        $userid = $USER->id;
       
        if(empty($userid)){ return 0;}
         $sql = "SELECT COUNT(rs.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}role r ON r.id=rs.roleid WHERE e.contextlevel=50 AND r.shortname ='student' AND rs.userid=$userid";
        
       $r = $DB->get_record_sql($sql);
   
       return $r->countrecord;
   }
   
   function is_teachear() {
    global $DB, $CFG,$USER;
        $userid = $USER->id;
        if(empty($userid)){ return 0;}
         $sql = "SELECT COUNT(rs.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}role r ON r.id=rs.roleid WHERE e.contextlevel=50  AND rs.userid=$userid AND r.shortname IN ('teacher','editingteacher','manager') ";
       $r = $DB->get_record_sql($sql);
       return $r->countrecord;
   }
   
   function count_enrol() {
    global $DB, $CFG,$USER;
        $userid = $USER->id;
         if (has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
				return -1;
		}
        if(empty($userid)){ return 0;}
         $sql = "SELECT COUNT(rs.id) AS countrecord FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}role r ON r.id=rs.roleid WHERE e.contextlevel=50  AND rs.userid=$userid";
        
       $r = $DB->get_record_sql($sql);
   
       return $r->countrecord;
   }

   function get_enrol_course_shortname_roles($userid=null) {
        global $DB, $CFG,$USER;
        if(empty($userid)){
            $userid = $USER->id;
        }
       
        if(empty($userid)){ return null;}
         $sql = "SELECT  DISTINCT r.shortname FROM {$CFG->prefix}role_assignments rs INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id INNER JOIN {$CFG->prefix}role r ON r.id=rs.roleid WHERE e.contextlevel=50  AND rs.userid=:userid";
        $fapram=array('userid'=>$userid);
         $r = $DB->get_records_sql($sql,$fapram);
   
       return $r;
   }
}
