<?php

require_once("$CFG->dirroot/theme/badiumview/locallib/course/contentlib.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/course/accesslib.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/utildata.php");
/**
 * Class theme_badiumview_factorycoursedata
 */
class theme_badiumview_factorycoursedata {
	
	  /**
     * get list of course topics
	 
     *
     * @param $courseid
     * @param null $section
     * @return array|null
     * @throws dml_exception
     */
    public function getTopics($param) {
		 $utildata=new theme_badiumview_utildata();
      
	    $userid   = $utildata->getVaueOfArray($param,'userid');
        $courseid = $utildata->getVaueOfArray($param,'courseid');
        $addactivities=$utildata->getVaueOfArray($param,'addactivities');
				
		$contentlib=new theme_badiumview_course_contentlib();
		
          //$param=array('courseid'=>$courseid,'visible'=>1);
          $list = $contentlib->get_topics($param);
          $list=$contentlib->add_default_topic_name($list);
		  
		  
		  
		  //get_list_item
		  if($addactivities){
			  $listnew=array();
			  $activities=$this->getActivities($param);
			  
			  //last id activity accessed
			  $accesslib=new theme_badiumview_course_accesslib();
			  $listlastaccessativities= $accesslib->get_lastaccess_ativities($courseid,$userid);
			  $lastactivityidaccessed=$accesslib->get_ativityid_lastaccessed($listlastaccessativities);
			  
			  foreach ($list as $trow) {
				$tsection=$utildata->getVaueOfArray($trow,'id'); 
				$activitiesnew=array();
				$countactivity=0; 
				$countactivitycompleted=0;
				$progresspercentnumber=0;
				$progresspercentnumberformat=0;
				$countactivityinprogress=0;
				$haslastactivityaccessed=0;
				foreach ( $activities as $arow) {
					
					$asection=$utildata->getVaueOfArray($arow,'section');
					$completed=$utildata->getVaueOfArray($arow,'completed');
					$tstatus=$utildata->getVaueOfArray($arow,'status');
					$completionview=$utildata->getVaueOfArray($arow,'completionview');
						 if($tsection==$asection){ 
							if($completionview > 0){
								$countactivity++;
								if($completed){$countactivitycompleted++;}
								if($tstatus=='inprogress'){$countactivityinprogress++;}
							}
							//check if last accessed
							$activityid=$utildata->getVaueOfArray($arow,'id');
							if($activityid==$lastactivityidaccessed){$haslastactivityaccessed=1;}
							$trow['activities']=array_push($activitiesnew, $arow);
						}
					  
					  
					}
				 $trow['activities']=$activitiesnew;
				 $trow['lastaccessed']=$haslastactivityaccessed;
				 $trow['showactivities']="";
				 if($haslastactivityaccessed){$trow['showactivities']=" in ";}
				 //percent progress
				
				if($countactivity >=0 && $countactivitycompleted >=0 && $countactivity >= $countactivitycompleted){
					if($countactivity==0){$progresspercentnumber=0;}
					else{
						$progresspercentnumber= $countactivitycompleted*100/$countactivity;
						$progresspercentnumberformat=number_format($progresspercentnumber, 2, ',', '.');
					}
              
				} 
				$status='';  
				$trow['completed']=0;
				if($progresspercentnumber==100){$trow['completed']=1;}
				$trow['countactivity']=$countactivity;
				$trow['countactivitycompleted']=$countactivitycompleted;
				$trow['progresspercentnumber']=$progresspercentnumber;
				$trow['progresspercentnumberformat']=$progresspercentnumberformat;
				//$trow['progresspercentinfo']="Atividades concluídas  $countactivitycompleted de $countactivity ($progresspercentnumberformat%)";
				$trow['progresspercentinfo']=" $countactivitycompleted de $countactivity ($progresspercentnumberformat%)";
				
				//status
				
				$trow['status']=''; //completed | inprogress | notstarted
				$trow['statusicon']='';
				if($progresspercentnumber==100){$trow['status']='completed';$trow['statusicon']='fa fa-check-circle-o';}
				else if($countactivityinprogress > 0){$trow['status']='inprogress'; $trow['statusicon']='fa fa-circle-thin';}
				else if($countactivityinprogress = 0){$trow['status']='notstarted';$trow['statusicon']='fa fa-circle-thin';}
				 array_push($listnew, $trow);	 
			  }
			  
			  $list=$listnew;
		  }
		 
		/*echo "<pre>";
		print_r($list);
		echo "</pre>";exit;*/
		  return $list;
		
	}

	
	/**
     * Get course activities.
     *
     * @return array
     * @throws coding_exception
     * @throws dml_exception
     */
    public function getActivities($param) {
		global $CFG;
        $utildata=new theme_badiumview_utildata();
        $userid   = $utildata->getVaueOfArray($param,'userid');
        $courseid = $utildata->getVaueOfArray($param,'courseid');
		$section = $utildata->getVaueOfArray($param,'section');
        $addactivitiesitemhtml=$utildata->getVaueOfArray($param,'addactivitiesitemhtml');
		$countaccess=$utildata->getVaueOfArray($param,'countaccess');
		
		
		if(empty($courseid)){return array();}
        
        $dcourse     = new stdClass();
        $dcourse->id = $courseid;
        $cmodinfo     = get_fast_modinfo($dcourse);

        $contentlib=new theme_badiumview_course_contentlib();
        $activities =$contentlib->get_list($courseid, $section);
        $activities = $this->castListactivitiesToArray($activities);
        
        $listactivitiescompletd = $contentlib->get_list_completed($userid, $courseid);
        $listactivityanableds   = $contentlib->get_enable_ativities($courseid);
        
		$listactivitieshtml=array();
        if($addactivitiesitemhtml){
			$listactivitieshtml=$contentlib->get_list_item($courseid, $section);
	    }
	   
		$activitiesnew=array();
		
		//count access
		$listactivitiseaccess=array();
		if($countaccess){
			$listactivitiseaccess=$contentlib->get_ativities_access_by_user($courseid,$userid);
			$listactivitiseaccessnew=array();
			foreach ($listactivitiseaccess as $lrow) {
				$acmi=$lrow->cmid;
				$cmidcountaccess=$lrow->countaccess;
				$listactivitiseaccessnew[$acmi]=$cmidcountaccess;
			}
			$listactivitiseaccess=$listactivitiseaccessnew;
		}
        foreach ($activities as $row) {
            $id = $utildata->getVaueOfArray($row,'id');
			$visible = $utildata->getVaueOfArray($row,'visible');
			$visibleold = $utildata->getVaueOfArray($row,'visibleold');
			$module = $utildata->getVaueOfArray($row,'module');
			$name = $utildata->getVaueOfArray($row,'name');
			$completionview = $utildata->getVaueOfArray($row,'completionview');
            
			//add url
			$url=$CFG->httpswwwroot."/mod/$module/view.php?id=$id";
			$row['url']=$url;
			
			//add link
			$link="<a href=\"$url\">$name</a>";
			$row['link']=$link;
			
			$row['html']=null;
			 if($addactivitiesitemhtml && isset($listactivitieshtml[$id])) {
				 $row['html']=$listactivitieshtml[$id];
			 }
			 //icon 
			 $row['urlicon']=$CFG->httpswwwroot."/mod/$module/pix/icon.svg";
			$conditionenabled = false;

			if (isset($listactivityanableds[$id])) {
                $conditionenabled = true;
            }
			
            if ($visible == 1 && $visibleold == 1 && $conditionenabled) {
               
                $row['locked'] = 1;
                $cml = $cmodinfo->get_cm($id);
                if ($cml->uservisible) {
                    $row['locked'] = 0;
                }
                $row['availableinfo'] =\core_availability\info::format_info($cml->availableinfo, $dcourse);
                

                if (array_key_exists($id, $listactivitiescompletd)) {
                    $row['completed']=1;
                } else {
                    $row['completed']=0;
					if($completionview==0){$row['completed']=-1;}
                }

               
            }
			$row['countaccess']=null;
			
			//count access
			if (array_key_exists($id, $listactivitiseaccess)) {$row['countaccess']=$listactivitiseaccess[$id];}
			
			//status
			$row['status']=null;
			$row['statuslabel']=null;
			$row['statusicon']=null;
			$countaccess=$utildata->getVaueOfArray($row,'countaccess');
			$completed=$utildata->getVaueOfArray($row,'completed');
			$locked=$utildata->getVaueOfArray($row,'locked');
			
			if($completed==1){
				$row['status']='completed';
				$row['statuslabel']='Concluido';
				$row['statusicon']='fa fa-check-circle';
			}else if($completed==-1){
				$row['status']='none';
				$row['statuslabel']='Sem rastreamento de conclusão';
				$row['statusicon']='fa fa-times-circle-o';
				
			}
			else if ($countaccess > 0){
				$row['status']='inprogress';
				$row['statuslabel']='Em andamento';
				$row['statusicon']='fa fa-circle';
			}else if ($countaccess == 0 ){
				$row['status']='notstarted';
				$row['statuslabel']='Não iniciado';
				$row['statusicon']='fa fa-circle';
			}
			
			if($locked){
				$row['status']='loked';
				$row['statuslabel']='Não liberado';
				$row['statusicon']='fa fa-lock';
				
			} 
			 array_push($activitiesnew, $row);
			
        }

        return $activitiesnew;
    }
	
	
	/**
     * Cast list activities to array.
     *
     * @param $result
     * @return array
     */
    private function castListactivitiesToArray($result) {
        $array = array();
        foreach ($result as $key => $value) {
            if (is_array($value)) {
                $array[$key] = $this->castListactivitiesToArray($value);
            }
            else {
                if (is_object($value)) {
                    $array[$key] = $this->castListactivitiesToArray($value);
                } else {
                    $array[$key] = $value;
                }
            }
        }
        return $array;
    }
	
		/**
     * Cast getCourseProgress
     *
     * @param $result
     * @return array
     */
     function getCourseProgress($listtopicwithactivities) {
		 $utildata=new theme_badiumview_utildata();
		  $coursecountactivity=0; 
		  $coursecountactivitycompleted=0;
		  $courseprogresspercentnumber=0;
		  $courseprogresspercentnumberformat=0;
		  
		  
		  foreach ($listtopicwithactivities as $trow) {
			 
			  $activities=$utildata->getVaueOfArray($trow,'activities');
			  foreach ( $activities as $arow) {
					$completed=$utildata->getVaueOfArray($arow,'completed');
					if($completed >=0){$coursecountactivity++;}
					if($completed >0){$coursecountactivitycompleted++;}
				}
		
		  }
		  
		  //course percent progress
				if($coursecountactivity >=0 && $coursecountactivitycompleted >=0 && $coursecountactivity >= $coursecountactivitycompleted){
					if($coursecountactivity==0){$courseprogresspercentnumber=0;}
					else{
						$courseprogresspercentnumber= $coursecountactivitycompleted*100/$coursecountactivity;
						$courseprogresspercentnumberformat=number_format($courseprogresspercentnumber, 2, ',', '.');
					}
              
				}
			$icourse=array();
			$icourse['countactivity']=$coursecountactivity;
			$icourse['countactivitycompleted']=$coursecountactivitycompleted;
			$icourse['progresspercentnumber']=$courseprogresspercentnumber;
			$icourse['progresspercentnumberformat']=$courseprogresspercentnumberformat;
			$icourse['progresspercentinfo']=" $coursecountactivitycompleted de $coursecountactivity ($courseprogresspercentnumberformat%)";
			
			return $icourse;
	}
}
