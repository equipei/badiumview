<?php

class theme_badiumview_core_jsonutil {
   
    function decode($data, $outarray = true) {
        $max_int_length = strlen((string) PHP_INT_MAX) - 1;
        $json_without_bigints = preg_replace('/:\s*(-?\d{' . $max_int_length . ',})/', ': "$1"', $data);
        $oresult = json_decode($json_without_bigints, $outarray);
        return $oresult;
    }

    public function encode($input) {
        $json = json_encode($input);
        return $json;
    }

    function escape($value) {
        $escapers = array("\\", "/", "\"",  "'", "\n", "\r", "\t", "\x08", "\x0c");
        $replacements = array("\\\\", "\\/", "\\\"", "\'","\\n", "\\r", "\\t", "\\f", "\\b");
        $result = str_replace($escapers, $replacements, $value);
        return $result;
    }
    


}

?>
