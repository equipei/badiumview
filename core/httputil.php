<?php

class theme_badiumview_core_httputil {

     function clean_uri(){
        $uri=$_SERVER['REQUEST_URI'];
        global $CFG;
        $host=$CFG->wwwroot;
        $p=explode("//",$host);
        if(!isset($p[1])){ return $uri;}
        $shost=explode("/",$p[1]);
        $suri=explode("/",$uri);
         $cont=0;
        $newuri="";
        foreach ($suri as $key => $value) {
              $phost=null;
               if(isset($shost[$key])){$phost=$shost[$key];}
               if($key > 0 ){
                   if(empty($value)){$newuri.='/';$cont++;}
                   else if($phost!=$value){
                       if($newuri!='/'){$newuri.='/'.$value;$cont++;}
                   }
                }
             }
        if($cont==0){ return $uri;}
       return $newuri;
    }
    function get_current_url($withoutquerystring=true){
         global $CFG;
         $uri=$this->clean_uri();
         if($withoutquerystring){
            $query=$_SERVER['QUERY_STRING'];
            $query="?$query";
            $uri=str_replace($query,"",$uri);
         }
         $url=$CFG->wwwroot.$uri;
         return $url;
    }

}