<?php
require_once("$CFG->dirroot/theme/badiumview/core/httpquerystring.php");
require_once("$CFG->dirroot/theme/badiumview//core/httputil.php");
require_once("$CFG->dirroot/theme/badiumview//core/jsonutil.php");
require_once("$CFG->dirroot/theme/badiumview/locallib/utildata.php");
class theme_badiumview_core_dblib
{
    private $table;
	private $utildata;
    function __construct($table = null)
    {
        if (!empty($table)) {
            $this->table = $table;
        }
		$this->utildata=new theme_badiumview_utildata();
    }
 
    function save($dto)
    {
        global $DB;
        $dto->timecreated = time();
        return $DB->insert_record($this->getTable(), $dto);
    }

    function edit($dto)
    {
        global $DB;
        $dto->timemodified = time();
        return $DB->update_record($this->getTable(), $dto);
    }

    function add($dto)
    {

        $dto = $this->add_default_value($dto);
        if (!empty($dto->id)) {
            $this->edit($dto);
            $rs = $dto->id;
        } else {
            $dto->id=$this->save($dto);
		 }
		$dto=$this->exec_after($dto);
        return $dto;
    }
    function add_default_value_to_form($dto)
    {

        return $dto;
    }
	function exec_after($dto)
    {

        return $dto;
    } 
    function  managelink($row, $param = array())
    {
        $outhtml = "";
        if (empty($row)) {
            return $outhtml;
        }
        if (!is_object($row)) {
            return $outhtml;
        }
        $id = $row->id;
        $labelview = get_string('view', 'theme_badiumview');
        $labeledit = get_string('edit', 'theme_badiumview');
        $labelremove = get_string('remove', 'theme_badiumview');
        $urlview = "index.php?id=$id&_opkey=viewrowdetails";
        $urledit = "edit.php?id=$id";
        $urlremove = "edit.php?id=$id&_opkey=removerowbyidconfirm";
        //$outhtml.="<a href=\"$urlview\"> $labelview | </a>";
        $outhtml .= "<a href=\"$urledit\"> $labeledit | </a>";
        $outhtml .= "<a href=\"$urlremove\"> $labelremove  </a>";
        return $outhtml;
    }
    function paging($countrows, $param)
    {
        global $OUTPUT, $CFG;
        $httpquerystring = new theme_badiumview_core_httpquerystring();
        $httpquerystring->setParam((array)$param);
        $httputil = new theme_badiumview_core_httputil();
        $httpquerystring->makeParam();
        $httpquerystring->remove('page');
        $httpquerystring->makeQuery();
        $newquery = $httpquerystring->getQuery();
        $url = $httputil->get_current_url();

        echo  $OUTPUT->paging_bar($countrows, $param->page, $param->perpage, "$url?$newquery");
    }
    function exist($dto)
    {

        return false;
    }

    function exist_shortname($dto, $isedit = false)
    {
        global $DB;
        global $CFG;
        if (empty($dto->shortname)) {
            return false;
        }
        if (!$isedit) {
            $sql = "SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}" . $this->getTable() . "  WHERE shortname=:shortname ";
            $result = $DB->get_record_sql($sql, array('shortname' => $dto->shortname));
            if (!empty($result)) {
                return $result->countrecord;
            }
        } else if ($isedit) {
            if (empty($dto->id)) {
                return false;
            }
            $sql = "SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}" . $this->getTable() . "  WHERE shortname=:shortname AND id!= :id ";
            $result = $DB->get_record_sql($sql, array('shortname' => $dto->shortname, 'id' => $dto->id));
            if (!empty($result)) {
                return $result->countrecord;
            }
        }
        return false;
    }
    function add_default_value($dto)
    {
        return $dto;
    }
    function delete_by_id($id)
    {
        global $DB;
        $result = $DB->delete_records($this->getTable(), array('id' => $id));
        return $result;
    }
    function get_by_id($id)
    {
        global $DB, $CFG;
        if (empty($id)) {
            return null;
        }
        $sql = "SELECT * FROM {$CFG->prefix}" . $this->getTable() . "  WHERE id = $id";
        $result = $DB->get_record_sql($sql);
        return $result;
    }
    function get_by_shortname($shortname)
    {
        global $DB, $CFG;
        if (empty($shortname)) {
            return null;
        }
        $sql = "SELECT * FROM {$CFG->prefix}" . $this->getTable() . " WHERE shortname =:shortname";
        $result = $DB->get_record_sql($sql, array('shortname' => $shortname));
        return $result;
    }
    function search_param($param)
    {

        return null;
    }
    function search_count($param)
    {

        return null;
    }

    function search($param)
    {

        return null;
    }
    function view($dto, $param)
    {

        return null;
    }
    function getTable()
    {
        return $this->table;
    }

    function setTable($table)
    {
        $this->table = $table;
    }
	
	
    public function getUtildata() {
        return $this->utildata;
    }

    public function setUtildata($utildata) {
        $this->utildata = $utildata;
    }
}
