<?php
defined('MOODLE_INTERNAL') || die();

function  xmldb_theme_badiumview_upgrade($oldversion) {
      global $CFG, $THEME, $DB;
      $dbman = $DB->get_manager();
      
       if ($oldversion < 2021040700) {
        xmldb_theme_badiumview_create_table_config();
		
        upgrade_plugin_savepoint(true, 2021040700, 'theme', 'badiumview');
      
       }
        return true;
}

function  xmldb_theme_badiumview_create_table_config(){
		global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('theme_badiumview_config');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }
		
		 //dtype
        $field = new xmldb_field('dtype', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL,null, null, 'id');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//instanceid
        $field = new xmldb_field('instanceid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'dtype');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		 
		 //name
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL,null, null, 'instanceid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//value
		 $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		 //tcontent
        $field = new xmldb_field('tcontent', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL,null, null, 'value');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//dconfig
		 $field = new xmldb_field('dconfig', XMLDB_TYPE_TEXT, null, null, null, null, 'tcontent');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		   //description
          $field = new xmldb_field('description', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'dconfig'); 
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'description');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //timemodified
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		$index = new xmldb_index('dtype');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('dtype'));
         if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		 
	    $index = new xmldb_index('instanceid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('instanceid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

		$index = new xmldb_index('name');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

		 //add index  
         $index= new xmldb_index('dtype_instanceid_name');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('dtype','instanceid','name'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

}
?>