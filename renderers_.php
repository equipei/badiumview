<?php

defined('MOODLE_INTERNAL') || die();

class theme_badiumview_core_renderer extends core_renderer {
  
 /**
     * Returns standard main content placeholder.
     * Designed to be called in theme layout.php files.
     *
     * @return string HTML fragment.
     */
    public function main_content() {
		return null;
        // This is here because it is the only place we can inject the "main" role over the entire main content area
        // without requiring all theme's to manually do it, and without creating yet another thing people need to
        // remember in the theme.
        // This is an unfortunate hack. DO NO EVER add anything more here.
        // DO NOT add classes.
        // DO NOT add an id.
		
        return '<div role="main">'.$this->unique_main_content_token.'</div>';
    }
	   public function region_main_settings_menu() {}
}

