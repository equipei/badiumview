<?php
class theme_badiumview_config_util {
    
    public function get_contenttype_options() {
		$list=array();
		$list['html']=get_string('configdtypehtml', 'theme_badiumview');
		$list['file']=get_string('configdtypefile', 'theme_badiumview');
		$list['text']=get_string('configdtypetext', 'theme_badiumview');
		$list['json']=get_string('configdtypejson', 'theme_badiumview');
		return $list;
	}
	public function get_contenttype_label($value){
		$label="";
		if($value=='html'){$label=get_string('configdtypehtml', 'theme_badiumview');}
		else if($value=='file'){$label=get_string('configdtypefile', 'theme_badiumview');}
		else if($value=='text'){$label=get_string('configdtypetext', 'theme_badiumview');}
		else if($value=='json'){$label=get_string('configdtypejson', 'theme_badiumview');}
		return $label;
	}

	public function get_context_options() {
		$list=array();
		$list['system']=get_string('contextsystem', 'theme_badiumview');
		$list['coursecategory']=get_string('contextcoursecategory', 'theme_badiumview');
		$list['course']=get_string('contextcourse', 'theme_badiumview');
		return $list;
	}
	public function get_context_label($value){
		$label="";
		if($value=='system'){$label=get_string('contextsystem', 'theme_badiumview');}
		else if($value=='coursecategory'){$label=get_string('contextcoursecategory', 'theme_badiumview');}
		else if($value=='course'){$label=get_string('contextcourse', 'theme_badiumview');}
		return $label;
	}
}
