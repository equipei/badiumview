<?php
require_once("$CFG->dirroot/theme/badiumview/core/dblib.php");
require_once("$CFG->dirroot/theme/badiumview/app/config/util.php");
require_once("$CFG->libdir/filelib.php");
class theme_badiumview_app_config_dblib extends theme_badiumview_core_dblib {
    
    function __construct($table="theme_badiumview_config") {
        parent::__construct($table);
    }


	 function add_default_value_to_form($dto) {
		if(!isset($dto->id)){$dto->id='';}
		if(!isset($dto->name)){$dto->name='';}
		if(!isset($dto->dconfig)){$dto->dconfig='';}
		if(!isset($dto->tcontent)){$dto->tcontent='';}
		if(!isset($dto->instanceid )){$dto->instanceid='';}
		if(!isset($dto->dtype)){$dto->dtype='';}
		
		
		if(!isset($dto->description)){$dto->description='';}
		else{
			if(!empty($dto->description)){
				$msg=array ('text'=>$dto->description,'format' => 1 );
				$dto->description=$msg;
			}
		}
		if(!isset($dto->value)){$dto->value='';}
		if(!isset($dto->valuetext)){$dto->valuetext='';}
		
		if($dto->tcontent=='json' || $dto->tcontent=='text'){
			$dto->valuetext=$dto->value;
		}
		
		if(!isset($dto->timecreated)){$dto->timecreated='';}
	  return $dto;
 }
   function exec_after($dto){
	   if($dto->tcontent=='json' || $dto->tcontent=='text'){return null;}
	    $context = context_system::instance();
        $options = ['trusttext' => true, 'subdirs' => false, 'maxfiles' => -1, 'maxbytes' => 0, 'context' => $context];

	    $dto     = file_postupdate_standard_editor($dto, 'value', $options, $context, 'theme_badiumview', $dto->tcontent, $dto->id);
		$dto     = $this->edit($dto);
       return $dto;
   }
    function search_param($param) {
        $wsql = ""; 
        if (!empty($param->name)) {
            $name  = strtolower($param->name);
            $wsql .= " AND LOWER(c.name) LIKE '%'".$name."'%'";
        }
        return  $wsql;
    }
   function add_default_value($dto){ 
  
	   if(isset($dto->description['text'])){$dto->description=$dto->description['text'];}
	   if(isset($dto->value_editor['text'])){$dto->value=$dto->value_editor['text'];}
	    
		if($dto->tcontent=='json' || $dto->tcontent=='text'){
			$dto->value=$dto->valuetext;
			unset($dto->valuetext);
		}
		if($dto->dtype=='system'){$dto->instanceid=1;}
		return $dto;
   } 
   
   
    function search_count($param) {
        global $DB, $CFG;
        $wsql   = $this->search_param($param);
        $sql    = "SELECT COUNT(c.id) as countrecord  FROM {$CFG->prefix}theme_badiumview_config c WHERE c.id > 0  $wsql ";
        $result = $DB->get_record_sql($sql);
        if (!empty($result)) {
            $result = $result->countrecord;
        }
        return $result;
    }

    
    function search($param) {
        global $DB, $CFG;
        $wsql=$this->search_param($param);
        $sql ="SELECT c.id,c.name,c.dtype,c.value,c.instanceid,c.tcontent,c.dconfig,c.description FROM {$CFG->prefix}theme_badiumview_config c  WHERE c.id > 0  $wsql ORDER BY c.name ";
        $result= $DB->get_records_sql($sql,null,$param->page*$param->perpage, $param->perpage);
        return $result;
    }

   
	function get_value($param) {
		
		$context=$this->getUtildata()->getVaueOfArray($param,'dtype');
		if(empty($context)){$context='system';}
		$context='system';
		$instanceid=$this->getUtildata()->getVaueOfArray($param,'instanceid');
		if(empty($instanceid)){$instanceid=1;}
		
		$name=$this->getUtildata()->getVaueOfArray($param,'name');
		if(empty($name)){return null;}
		
		$fparam=array('dtype'=>$context,'instanceid'=>$instanceid,'name'=>$name);
		
        global $DB, $CFG;
		$sql ="SELECT c.tcontent,c.value,c.dconfig FROM {$CFG->prefix}theme_badiumview_config c  WHERE c.name = :name AND c.dtype=:dtype AND c.instanceid =:instanceid";
        $result= $DB->get_record_sql($sql,$fparam);
        return $result;
    }

	function get_values($param) {
		$wsql="";
		$fparam=array();
		$context=$this->getUtildata()->getVaueOfArray($param,'dtype');
		if(!empty($context)){
			$wsql.=" AND c.dtype=:dtype ";
			$fparam['dtype']=$context;
		}
	
		$instanceid=$this->getUtildata()->getVaueOfArray($param,'instanceid');
		if(!empty($instanceid)){
			$wsql.=" AND c.instanceid =:instanceid ";
			$fparam['instanceid']=$instanceid;
		}
	
		$name=$this->getUtildata()->getVaueOfArray($param,'name');
		if(!empty($name)){
			$operatorname=$this->getUtildata()->getVaueOfArray($param,'_operatorname');
			if($operatorname=='LIKEEND'){
				$wsql.=" AND c.name LIKE :name ";
				$fparam['name']=$name."%";
			}else{
				$wsql.=" AND  c.name = :name ";
				$fparam['name']=$name;
			}
			
		}
	
		global $DB, $CFG;
		$sql ="SELECT c.id,c.name,c.tcontent,c.value,c.dconfig FROM {$CFG->prefix}theme_badiumview_config c  WHERE c.id > 0  $wsql";
		  $result= $DB->get_records_sql($sql,$fparam);
        return $result;
    }
	function replace_editor_expressaion_file($content, $itemid, $filearea) {
        $context = context_system::instance();
        return file_rewrite_pluginfile_urls($content, 'pluginfile.php',$context->id, 'theme_badiumview', $filearea, $itemid);
    }

}
