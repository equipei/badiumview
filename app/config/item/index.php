<?php
require_once("../../../../../config.php");
require_once("$CFG->dirroot/theme/badiumview/app/config/item/dblib.php");

require_login();

$param               = new stdClass;
$param->page         = optional_param('page', 0,PARAM_INT);
$param->perpage      = optional_param('perpage', 10,PARAM_INT);
$param->name         = optional_param('name', null,PARAM_TEXT);
$param->id        = optional_param('id', 0, PARAM_INT);
$param->opkey        = optional_param('_opkey', NULL, PARAM_TEXT);

$context = context_system::instance();  
$PAGE->set_context(context_system::instance());
if (!has_capability('moodle/site:config', $context)){
    require_capability('moodle/site:config', $context , NULL, false);
}
$PAGE->set_heading(get_string('configitemmanager', 'theme_badiumview'));
$PAGE->set_title(get_string('configitemmanager', 'theme_badiumview'));
$PAGE->set_url('/theme/badiumview/app/config/item/index.php');

$PAGE->navbar->add(get_string('pluginname','theme_badiumview'),new moodle_url("$CFG->httpswwwroot/theme/badiumview/app/config/item/index.php"));
$PAGE->navbar->add(get_string('configitemmanager','theme_badiumview'));

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('configitemmanager', 'theme_badiumview'));

$dto   = new stdClass;
$dblib = new theme_badiumview_app_config_item_dblib();

$dto->countrecord = $dblib->search_count($param);
$dto->rows        = $dblib->search($param);

echo $dblib->view($dto, $param);

echo $OUTPUT->footer();

