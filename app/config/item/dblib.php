<?php
require_once("$CFG->dirroot/theme/badiumview/app/config/dblib.php");
require_once("$CFG->dirroot/theme/badiumview/app/config/util.php");
class theme_badiumview_app_config_item_dblib extends  theme_badiumview_app_config_dblib {
    
    function __construct() {
        parent::__construct();
    }


    function view($dto, $param) {
		
		 if(isset($param->opkey) && $param->opkey=='viewrowdetails' ){return null;}
        global $CFG;
       

        $table        = new html_table();
        $table->head  = [get_string('id', 'theme_badiumview'), get_string('level', 'theme_badiumview'),get_string('key', 'theme_badiumview'),get_string('value', 'theme_badiumview'), get_string('description', 'theme_badiumview'),''];
        $table->align = ['left', 'left', 'left', 'left', 'left', 'left', 'right'];
        $table->class = 'generaltable table';
        $table->data  = [];

        // Make table rows. 
        foreach ($dto->rows as $row) {
            $status='';//$util->get_status_label($row->sstatus);
            $link=$this->managelink($row);  
			$row->value=$this->replace_editor_expressaion_file($row->value, $row->id, $row->tcontent);
            $table->data[] = array($row->id, $row->dtype,$row->name,$row->value,$row->description,$link);
        }
		$linkaddnew="<a href=\"edit.php\">".get_string('addnew', 'theme_badiumview')."</a> <br />";
        echo $linkaddnew;
		echo get_string('countrecord', 'theme_badiumview') . $dto->countrecord;
        $this->paging($dto->countrecord, $param);
        echo html_writer::table($table);
        $this->paging($dto->countrecord, $param);
    }


}
