<?php
require_once $CFG->libdir . '/formslib.php';
require_once("$CFG->dirroot/theme/badiumview/app/config/util.php");
class theme_badiumview_app_config_item_form extends moodleform {

	/**
	 * Form definition.
	 *
	 * @throws coding_exception
	 * @throws dml_exception
	 */
	function definition() {
		global $dto;
		
		$mform        = &$this->_form;
		$renderer     = &$mform->defaultRenderer();
		$util=new theme_badiumview_config_util();
		
		$context      = context_system::instance();
        $feditorconf = ['trusttext' => true, 'subdirs' => false, 'maxfiles' => -1, 'maxbytes' => 0, 'context' => $context];
		
		$mform->addElement('header', 'general', get_string('general', 'theme_badiumview'), 'general');

		if (isset($dto->id) && $dto->id != null && $dto->id > 0) {
			$mform->addElement('hidden', 'id', $dto->id);
			$mform->setType('id', PARAM_INT);

		}
		
		$mform->addElement('text', 'name', get_string('key', 'theme_badiumview'),'size="70"',['class' => 'fitem_id_name']);
		$mform->setType('name', PARAM_TEXT);
		$mform->addRule('name', get_string('requiredfield', 'theme_badiumview') , 'required', null, 'cliente');
		$mform->setDefault('name', $dto->name);
		
		$mform->addElement('select', 'tcontent', get_string('configdtype','theme_badiumview'), $util->get_contenttype_options(),['class' => 'fitem_id_tcontent']);
        $mform->setType('tcontent', PARAM_TEXT);
        $mform->setDefault('tcontent', $dto->tcontent);
		
		$mform->addElement('editor', 'value_editor', get_string('value', 'theme_badiumview'),['class' => 'fitem_id_value_editor','rows' => 3],$feditorconf);
		$mform->setType('value_editor',  PARAM_RAW);
		//$mform->setDefault('value_editor', $dto->value); 
		
		$mform->addElement('textarea', 'valuetext', get_string('value', 'theme_badiumview'),['class' => 'fitem_id_valuetext','rows' => 3,'cols'=>70]);
		$mform->setType('valuetext', PARAM_TEXT);
		$mform->setDefault('valuetext', $dto->valuetext);
	
		
		$mform->addElement('select', 'dtype', get_string('context','theme_badiumview'), $util->get_context_options());
        $mform->setType('dtype', PARAM_TEXT);
        $mform->setDefault('dtype', $dto->dtype);
		
		$mform->addElement('text', 'instanceid', get_string('instanceid', 'theme_badiumview'),['class' => 'fitem_id_instanceid','size'=>10]);
		$mform->setType('instanceid', PARAM_INT);
		$mform->addRule('instanceid', get_string('requiredfield', 'theme_badiumview') , 'required', null, 'cliente');
		$mform->setDefault('instanceid', $dto->instanceid);
		
		
		 
				
		$mform->addElement('header', 'otherconfig', get_string('otherconfig', 'theme_badiumview') , 'otherconfig');  
		$mform->setExpanded('otherconfig',false);
		$mform->addElement('textarea', 'dconfig', get_string('dconfig', 'theme_badiumview'),['class' => 'fitem_id_dconfig','rows' => 3,'cols'=>70]);
		$mform->setType('dconfig', PARAM_TEXT);
		$mform->setDefault('dconfig', $dto->dconfig);
		
		$mform->addElement('editor', 'description', get_string('description', 'theme_badiumview') , 'wrap="virtual" rows="3" cols="40"',['class' => 'fitem_id_description']);
		$mform->setType('description', PARAM_RAW);
		$mform->setDefault('description', $dto->description);
		
		$this->add_action_buttons(true, get_string('save', 'theme_badiumview'));
        $this->set_data($this->_customdata['fdata']); 
	}

	/**
	 * Form validation.
	 *
	 * @param array $data
	 * @param array $files
	 * @return array
	 */
	function validation($data, $files) {
		global $dblib;
		$errors = parent::validation($data, $files);
		$dto     = new stdClass();
		$dto->shortname=null;
		$dto->id=null;
		if(isset($data['shortname'])){$dto->shortname=$data['shortname'];}
		if(isset($data['id'])){$dto->id=$data['id'];}
		
		if (!empty($dto->shortname) && empty($dto->id)) {
			$exist=$dblib->exist_shortname($dto,false);
			if($exist){$errors['shortname'] = get_string('shortnameduplicated', 'theme_badiumview');}
		} else if (!empty($dto->shortname) && !empty($dto->id)) {
			$exist=$dblib->exist_shortname($dto,true);
			if($exist){$errors['shortname'] = get_string('shortnameduplicated', 'theme_badiumview');}
		} 
		return $errors;
	}
}
