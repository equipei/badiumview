<?php
require_once("../../../../../config.php");
require_once("$CFG->dirroot/theme/badiumview/app/config/item/dblib.php");
require_once("$CFG->dirroot/theme/badiumview/app/config/item/form.php");
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/badiumview/javascript/jquery-2.1.0.js') , true);
#$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/badiumview/app/config/item/form_js.php') , true);
require_login();
$param     = new stdClass;
$param->id        = optional_param('id', 0, PARAM_INT);
$param->opkey        = optional_param('_opkey', NULL, PARAM_TEXT);
$param->tokenexec        = optional_param('_tokenexec', NULL, PARAM_TEXT);


$dto     = new stdClass();
$dblib   = new theme_badiumview_app_config_item_dblib();

$msgaddsuccess = get_string('addsuccess', 'theme_badiumview');

$context = context_system::instance();
if (!has_capability('moodle/site:config', $context)){
    require_capability('moodle/site:config', $context , NULL, false);
}

$PAGE->set_context($context);
$PAGE->set_url('/theme/badiumview/app/config/item/edit.php');
$PAGE->navbar->add(get_string('configitemmanager', 'theme_badiumview'), new moodle_url("$CFG->httpswwwroot/theme/badiumview/app/config/item/edit.php"));
$PAGE->set_heading(get_string('configitemmanager', 'theme_badiumview'));
$PAGE->set_title(get_string('configitemmanager', 'theme_badiumview'));
$PAGE->navbar->add(get_string('pluginname', 'theme_badiumview'), new moodle_url("$CFG->httpswwwroot/theme/badiumview/app/config/item/index.php"));
//$PAGE->navbar->add(get_string('configitemadd', 'theme_badiumview'), new moodle_url("$CFG->httpswwwroot/theme/badiumview/app/config/item/index.php"));
$PAGE->navbar->add(get_string('configitemadd', 'theme_badiumview'));

if (!empty($param->id) && is_int($param->id)) {
    $dto = $dblib->get_by_id($param->id);
}
$dto = $dblib->add_default_value_to_form($dto);
$context = context_system::instance();

if (!empty($param->id) && is_int($param->id)) {
	$options = ['trusttext' => true, 'subdirs' => false, 'maxfiles' => -1, 'maxbytes' => 0, 'context' => $context];
	$dto = file_prepare_standard_editor($dto, 'value', $options, $context, 'theme_badiumview', $dto->tcontent, $dto->id);
}
$form     = new theme_badiumview_app_config_item_form(null, ['fdata' => $dto]);
deleterow($param, $dto, $dblib);
proccesform($dblib, $form);
pageview($form, $param);


function proccesform($dblib, $form)
{
    global $CFG;
    $urlindex = "$CFG->httpswwwroot/theme/badiumview/app/config/item/index.php";
    if ($form->is_cancelled()) {
        redirect($urlindex);
    } else if ($formdata = $form->get_data()) {
        $fresult = $dblib->add($formdata);
        $msgaddsuccess = get_string('addsuccess', 'theme_badiumview');
        if (!empty($formdata->id)) {
            $msgaddsuccess = get_string('editsuccess', 'theme_badiumview', $formdata->id);
        }
        redirect($urlindex, $msgaddsuccess, 2);
    }
}

function deleterow($param, $dto, $dblib)
{
    global $CFG;
    global $OUTPUT;
    $urlindex = "$CFG->httpswwwroot/theme/badiumview/app/config/item/index.php";
    $tokenexec = md5($dto->timecreated);
    if ($param->opkey == 'removerowbyidconfirm') {
        $id = $param->id;

        $urlremoveexec = "$CFG->httpswwwroot/theme/badiumview/app/config/item/edit.php?id=$id&_opkey=removerowbyidexec&_tokenexec=$tokenexec";

        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('configitemmanager', 'theme_badiumview'));
        echo $OUTPUT->confirm(get_string('removeconfirm', 'theme_badiumview', $dto->name), $urlremoveexec, $urlindex);
        echo $OUTPUT->footer();
    } else  if ($param->opkey == 'removerowbyidexec' && $param->tokenexec == $tokenexec) {
        if (!empty($dto->id)) {
            $dblib->delete_by_id($dto->id);
            redirect($urlindex, get_string('removesuccess', 'theme_badiumview', $dto->name), 2);
        }
        redirect($urlindex);
    }
}

function pageview($form, $param)
{
    if ($param->opkey == 'removerowbyidconfirm') {
        return null;
    }
	global $CFG;
    global $OUTPUT;
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('configitemmanager', 'theme_badiumview'));
    require_once("$CFG->dirroot/theme/badiumview/app/config/item/form_js.php");
	$form->display();
	
    echo $OUTPUT->footer();
}
