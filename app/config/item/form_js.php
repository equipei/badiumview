<script type="text/javascript">
    $( document ).ready(function() {
        $(function() {
            badiumviewFormChange();
            badiumviewFormRead();

        }); 

        function badiumviewFormRead() {
			//context type
            var dtype = $("#id_dtype").val();
			 if (dtype ==  'system') {
                $("#fitem_id_instanceid").hide();
				 $(".fitem_id_instanceid").hide();
            } else {
                $("#fitem_id_instanceid").show();
				$(".fitem_id_instanceid").show();
            }
			
			//type context
			  var tcontent = $("#id_tcontent").val();
			
			if (tcontent ==  'html') {
                $("#fitem_id_value_editor").show();
				$(".fitem_id_value_editor").show();
				$("#fitem_id_valuetext").hide();
				$(".fitem_id_valuetext").hide();
            } else if (tcontent ==  'file') {
                $("#fitem_id_value_editor").show();
				 $(".fitem_id_value_editor").show();
				$("#fitem_id_valuetext").hide();
				$(".fitem_id_valuetext").hide();
            }else {
                $("#fitem_id_value_editor").hide();
				$(".fitem_id_value_editor").hide();
				$("#fitem_id_valuetext").show();
				$(".fitem_id_valuetext").show();
            }
			
        }

        function badiumviewFormChange() {
            $('#id_dtype').change(function() {
                var dtype = $("#id_dtype").val();
				if (dtype ==  'system') {
					$("#fitem_id_instanceid").hide();
					$(".fitem_id_instanceid").hide();
				} else {
					$("#fitem_id_instanceid").show();
					$(".fitem_id_instanceid").show();
				}
	          });
			
			//type context
			 $('#id_tcontent').change(function() {
				  var tcontent = $("#id_tcontent").val();
			
				if (tcontent ==  'html') {
					$("#fitem_id_value_editor").show();
					$(".fitem_id_value_editor").show();
					$("#fitem_id_valuetext").hide();
					$(".fitem_id_valuetext").hide();
				} else if (tcontent ==  'file') {
					$("#fitem_id_value_editor").show();
					$(".fitem_id_value_editor").show();
					$("#fitem_id_valuetext").hide();
					$(".fitem_id_valuetext").hide();
				}else {
					$("#fitem_id_value_editor").hide();
					$(".fitem_id_value_editor").hide();
					$("#fitem_id_valuetext").show();
					$(".fitem_id_valuetext").show();
				}	
			 });	 
			
        }
    });
</script>
